#include "stdafx.h"
//回调事件均以"EID_Function"格式,其中"EID_"开头，"Function"是接收到此回调事件回调函数的名称
#define  EID_OnFrontDisconnected      0
#define  EID_OnFrontConnected         1
#define  EID_OnRspUserLogin_Scuess    2
#define  EID_OnRspUserLogin_Failer    3
#define  EID_OnRspUserLoginout_Scuess 4
#define  EID_OnRspUserLoginout_Failer 5
#define  EID_OnRtnDepthMarketData     6
#define  EID_IsErrorRspInfo           7
#define  EID_OnRspSubMarketData       8
#define  EID_OnRspUnSubMarketData     9
#define  EID_OnRspUserLogout          10

//trader
#define  EID_OnRspOrder                     11
#define  EID_OnRspTrade                     12     //add  2016.12.28
#define  EID_OnRtnInstrumentStatus          13     //add
#define  EID_OnRspQryAccountregister        14     //add 签约关系响应

#define  EID_OnRtnFromBankToFutureByFuture  15     //add
#define  EID_OnRtnFromFutureToBankByFuture  16     //add
#define  EID_OnRspQueryMaxOrderVolume       17     //add
#define  EID_OnRspSettlementInfoConfirm     18    //add

//回调类型
#define TD_SYSTEM_NONE                8000 //无
#define TD_LOGIN_SCUESS               8001 //登录成功
#define TD_LOGIN_DENIED               8002 //登录被拒绝
#define TD_LOGIN_ERRORPASSWORD        8003 //密码错误 ??
#define TD_LOGINOUT_SCUESS            8004 //登出成功
#define TD_LOGINOUT_DENIED            8005 //登录被拒绝    //ADD 2016.12.28
#define TD_NETCONNECT_SCUESS          8006 //连接成功
#define TD_NETCONNECT_BREAK           8007 //断开连接
#define TD_NETCONNECT_FAILER          8008 //连接失败 ??
//#define SYSTEM_SUBCRIBE_SCUESS      8008 //订阅成功
//#define SYSTEM_UNSUBCRIBE_SCUESS    8009 //取消订阅成功
//#define SYSTEM_NEWTICK              8010 //新Tick到来
#define TD_SYSTEM_ERROR               8011 //错误应答
#define TD_ORDER_INFO                 8012 //ORDER
#define TD_TRADE_INFO                 8013 //TRADER
#define TD_INSTRUMENT_STATUS          8014 //合约交易状态
#define TD_QUERY_ACCOUNTREGISTER      8015 //签约关系响应
#define TD_BYFUTURE_BANKTOFUTURE      8016 //期货发起银行资金转期货通知
#define TD_BYFUTURE_FUTURETOBANK      8017 //期货发起期货资金转银行通知
#define TD_QUERY_MAXORDERVOLUME       8018 //
#define TD_SETTLEMENTINFOCONFIRM      8019