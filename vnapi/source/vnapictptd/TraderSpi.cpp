/*
（1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
适合学习入门和拓展开发，就在1.0版发布后不久，我的vnapi 1.0（当时不叫vnapi）就遭受了非法金融经营者用1.2万字连载诽谤文的诋毁攻击，
他们甚至在诽谤文下方还放上他们自己的涉及非法金融开源软件的广告。
从那次诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？
当初遇此情形，我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
写出了virtualapi仿真回测（盛立版、CTP版、通联Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆。
由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
其产品甚至短暂的上了某期货公司官网。
在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，我忠心的希望金融从业者们牢记合规底线，
勿给非法金融可乘之机。
本次发布的vnapi2.10 期货CTP接口行情库底层代码，源于2017年4月的1.0开源版本，已更新至支持新版的CTP接口。
改名前的2.09 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
这6年时间，我们一边开发产品，一边开始了长达6年的维权之路。
vnapi2.0作为一款Python CTP接口的框架，基于Ctypes技术，简单易学，主要目的是为了为开发者提供一个通俗易懂
的后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。
本团队还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn），
面向的是专业机构客户，VNTrader系列均是在vnapi 1.0后端框架技术上迭代产生的，
当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于quant林和上海量贝软件的原创产品，
quant林于2014年获得基于AR增强现实技术的国家发明专利一项。
quant林另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。

（2）vnapi 遵循 开源协议GPL v3
简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
官方网站：http://www.vnpy.cn
*/
#include "stdafx.h"
#include "../common/common.h"
#include "def.h"
#include "TraderSpi.h"
#include <process.h>
#include <ShellApi.h>
#include <iostream> 
#include <fstream> 
using namespace std;
CRITICAL_SECTION g_csdata;

double YestayAllAmount = -999999999;
double TodayAllAmount = -999999999;
double UserAmount = -999999999;
/*
char InstrumentID_n[TYPE_NUM][10] =
{
	"ni1701", "rb1701","ag1612","m1701","y1701","zn1611","bu1612","ru1701","cs1701","jd1701","pp1701","i1701","al1611","au1612","p1701","CF701","TA701","MA701","FG701" ,"a1701"
};*/

bool FindStr(int id, char * str)
{
	//if (_stricmp(InstrumentID_n[id], str) == 0)
		return true;
	//else
	//	return false;
}

int SaveInstrumentID = { 0 };
bool  checkstate = false;
//bool  TypeCheckState_B_Today[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
//bool  TypeCheckState_S_Today[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

//bool  TypeCheckState_B_History[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
//bool  TypeCheckState_S_History[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
///请求查询投资者持仓响应
extern bool showpositionstate;

extern list <CMDCONTENT> cmdlist;
extern list <CThostFtdcOrderField> orderlist;
extern list <CThostFtdcTradeField> tradelist;
extern list <CThostFtdcAccountregisterField> qryaccountregisterlist;
extern list <CThostFtdcRspTransferField> banktofuturebyfuturelist;
extern list <CThostFtdcRspTransferField> futuretobankbyfuturelist;
extern list <CThostFtdcQryMaxOrderVolumeField>  querymaxordervolumelist;  
extern list <CThostFtdcInstrumentStatusField> InstrumentStatuslist;
extern list <CThostFtdcRspInfoField> errorlist;
extern list <CThostFtdcSettlementInfoConfirmField> settlementlist;
extern list <CThostFtdcRspUserLoginField> loginlist;
extern list <CThostFtdcUserLogoutField> loginoutlist;
extern list <int> connectlist;
///请求查询合约保证金率响应
extern list <CThostFtdcInstrumentMarginRateField> MarginRatelist;
//extern list <CThostFtdcInstrumentMarginRateField>::iterator MarginRate_Iter;
///请求查询合约手续费率响应
extern list <CThostFtdcInstrumentCommissionRateField> CommissionRatelist;
//extern list <CThostFtdcInstrumentCommissionRateField>::iterator CommissionRate_Iter;
extern HANDLE hEvent[MAX_EVENTNUM];
extern std::string gTDFrontAddr[3];
extern std::string gBrokerID;
extern std::string gUserID;
extern std::string gPassword;
extern HANDLE ghTradedVolMutex;
extern std::unordered_map<int, int> gOrderRef2TradedVol;
//乘数
std::unordered_map<std::string, double> gUnderlyingMultiple;
//保证金率
std::unordered_map<std::string, double> gMarginRate_long;
std::unordered_map<std::string, double> gMarginRate_short;
//手续费率
std::unordered_map<std::string, double> gCommissionRate;
//查询最大报单数量
std::unordered_map<std::string, int> gMaxOrderVolume;
//持仓map
std::unordered_map<std::string, int> gPosition_S;
std::unordered_map<std::string, int> gPosition_B;
std::unordered_map<std::string, int> gPosition_S_Today;
std::unordered_map<std::string, int> gPosition_B_Today;
std::unordered_map<std::string, int> gPosition_S_History;
std::unordered_map<std::string, int> gPosition_B_History;
std::unordered_map<std::string, int> gTypeCheckState_S_Today;
std::unordered_map<std::string, int> gTypeCheckState_B_Today;
std::unordered_map<std::string, int> gTypeCheckState_S_History;
std::unordered_map<std::string, int> gTypeCheckState_B_History;
CThostFtdcTraderApi *tdapi;
TraderSpi *tdspi;

TraderSpi::TraderSpi()
{
	bInitOK = false;
	iRequestID = 0;
	iOrderRef = 0;
	FRONT_ID = 0;
	SESSION_ID = 0;
	tdapi = NULL;
	hSyncObj = ::CreateEvent(NULL, FALSE, FALSE, NULL);
}

TraderSpi::~TraderSpi()
{
	if (tdapi)
	{
 		tdapi->Release();
 		tdapi = NULL;
	}
	::CloseHandle(hSyncObj);
}

bool GState = true;
DWORD WINAPI PositionThreadProc(void* p)	//更新排名
{
	while (true)
	{
		GState = !GState;
		if (GState)
		    tdspi->ReqQryInvestorPosition();		
		else
			 tdspi->ReqQryTradingAccount();
		Sleep(3000);
	}
}

DWORD WINAPI ReqQryInstrumentMarginRateThreadProc(void* p)
{
	tdspi->ReqQryInstrumentMarginRate("rb1701");		
	return 1;
}  

/*
void TraderSpi::ReqQryInvestorPosition()
{

	if (tdapi == NULL)
	{
		//cout << "--->>>ReqQryInvestorPosition指针错误" << endl;			  //指针检查
		//WirteTradeRecordToFileMainThread(0, "ReqQryInvestorPosition指针错误");
		return;
	}
	// WirteTradeRecordToFileMainThread(0, "ReqQryInvestorPosition");


	//CThostFtdcQryInvestorPositionField req = { 0 };
	//strcpy(req.BrokerID, m_BrokerID);
	//strcpy(req.InvestorID, m_InvestorInfos[reqInfo.lAccIdx].InvestorID);
	//req.InstrumentID; //指定合约的话，就是查询特定合约的持仓信息，不填就是查询所有持仓  
	//ReqQryInvestorPosition(&req, reqInfo.nRequestID);


	CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(req));
	//strcpy(req.BrokerID, BROKER_ID);
	//strcpy(req.InvestorID, INVESTOR_ID);
	//strcpy(req.InstrumentID, INSTRUMENT_ID);
	//printf("指定持仓%s", INSTRUMENT_ID);
	int iResult = tdapi->ReqQryInvestorPosition(&req, ++iRequestID);
	cerr << "--->>> 请求查询投资者持仓: " << ((iResult == 0) ? "成功" : "失败") << endl;
}

*/

bool TraderSpi::InitTd()
{
	std::cout << "vnapi(Trader.CTP for Python)2.10\n" <<std::endl;
	char dir[256] = {0};
	::GetCurrentDirectory(255, dir);
	std::string tempDir = std::string(dir).append(".\\CTP\\");
	::CreateDirectory(tempDir.c_str(), NULL);
	tdapi = CThostFtdcTraderApi::CreateFtdcTraderApi(".\\CTP\\");
	tdspi = this;
	tdapi->RegisterSpi(this);
	tdapi->SubscribePublicTopic(THOST_TERT_QUICK);
	tdapi->SubscribePrivateTopic(THOST_TERT_QUICK);
	tdapi->RegisterFront((char *)gTDFrontAddr[0].c_str());
	tdapi->RegisterFront((char *)gTDFrontAddr[1].c_str());
	tdapi->RegisterFront((char *)gTDFrontAddr[2].c_str());
	std::cout << "InitTd..." << std::endl;
	tdapi->Init();
	DWORD err = ::WaitForSingleObject(hSyncObj, 10000);
	if (err == WAIT_OBJECT_0)
		bInitOK = true;
	//查询持仓线程
	HANDLE hThread3 = ::CreateThread(NULL, 0, PositionThreadProc, NULL, 0, NULL);
	HANDLE hThread4 = ::CreateThread(NULL, 0, ReqQryInstrumentMarginRateThreadProc, NULL, 0, NULL);
	return bInitOK;
}

void TraderSpi::OnFrontConnected()
{
	CThostFtdcReqUserLoginField req;
	::ZeroMemory(&req, sizeof(req));
	_snprintf_s(req.BrokerID,sizeof(req.BrokerID), sizeof(req.BrokerID)-1,"%s", gBrokerID.c_str());
	_snprintf_s(req.UserID, sizeof(req.UserID), sizeof(req.UserID) - 1, "%s", gUserID.c_str());
	_snprintf_s(req.Password, sizeof(req.Password), sizeof(req.Password) - 1, "%s", gPassword.c_str());
	int re = tdapi->ReqUserLogin(&req, ++iRequestID);
	CMDCONTENT tn;
	memset(&tn, 0, sizeof(CMDCONTENT));
	strncpy_s(tn.content, sizeof(tn.content), "连接交易服务器成功", sizeof("连接交易服务器成功"));
	tn.cmd = TD_NETCONNECT_SCUESS;
	EnterCriticalSection(&g_csdata);
	cmdlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnFrontConnected]);
}

void TraderSpi::OnFrontDisconnected(int nReason)
{
	SYSTEMTIME t;
	::GetLocalTime(&t);
	std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
	std::cout << "--->>> Reason = " << nReason << std::endl;
	//::Beep(450, 10000);
	CMDCONTENT tn;
	memset(&tn, 0, sizeof(CMDCONTENT));
	strncpy_s(tn.content, sizeof(tn.content), "与交易服务器断开连接", sizeof("与交易服务器断开连接"));
	tn.cmd = TD_NETCONNECT_BREAK;
	EnterCriticalSection(&g_csdata);
	cmdlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnFrontDisconnected]);
}

//客户端认证响应
void TraderSpi::OnRspAuthenticate(CThostFtdcRspAuthenticateField *pRspAuthenticateField, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	//SYSTEMTIME t;
	//::GetLocalTime(&t);
	//std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
	//std::cout << "--->>> " << __FUNCTION__ << std::endl;
	//std::cout << "--->>> Reason = " << nReason << std::endl;
	//::Beep(450, 10000);
	//抄袭MD的
	//CMDCONTENT tn;
	//memset(&tn, 0, sizeof(CMDCONTENT));
	//strncpy_s(tn.content, sizeof(tn.content), "与交易服务器断开连接", sizeof("与交易服务器断开连接"));
	//tn.cmd = TD_NETCONNECT_BREAK;
	//EnterCriticalSection(&g_csdata);
	//cmdlist.push_back(tn);
	//LeaveCriticalSection(&g_csdata);
	//LeaveCriticalSection(&g_csdata);
	//SetEvent(hEvent[EID_OnFrontDisconnected]);
}

void TraderSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pRspUserLogin==NULL)return;
	FRONT_ID = pRspUserLogin->FrontID;
	SESSION_ID = pRspUserLogin->SessionID;
	//if (bIsLast)
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo && pRspInfo->ErrorID != 0)
		{
			std::cout << "Failer:OnRspUserLogin " << pRspInfo->ErrorID << pRspInfo->ErrorMsg<< "\n"<< std::endl;
			CThostFtdcRspUserLoginField tn;
			memset(&tn, 0, sizeof(CThostFtdcRspUserLoginField));
			memcpy_s(&tn,sizeof(CThostFtdcRspUserLoginField), pRspUserLogin,sizeof(CThostFtdcRspUserLoginField));
			//strncpy_s(tn.content, sizeof(tn.content), "登录行情服务器失败", sizeof("登录行情服务器失败"));
			//tn.cmd = TD_LOGIN_DENIED;
			EnterCriticalSection(&g_csdata);
			loginlist.push_back(tn);
			LeaveCriticalSection(&g_csdata);
			SetEvent(hEvent[EID_OnRspUserLogin_Failer]);
		}
		else
		{
			std::cout << "Scuess:OnRspUserLogin\n" <<  std::endl;
			CThostFtdcRspUserLoginField tn;
			memset(&tn, 0, sizeof(CThostFtdcRspUserLoginField));
			memcpy_s(&tn, sizeof(CThostFtdcRspUserLoginField), pRspUserLogin, sizeof(CThostFtdcRspUserLoginField));
			EnterCriticalSection(&g_csdata);
			loginlist.push_back(tn);
			LeaveCriticalSection(&g_csdata);
			SetEvent(hEvent[EID_OnRspUserLogin_Scuess]);
		    ReqSettlementInfoConfirm();
			Sleep(3000);
		}
	}
}

void TraderSpi::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pUserLogout==NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo && pRspInfo->ErrorID != 0)
		{
			printf("Failer:OnRspUserLogout,ErrorID=0x%04x, ErrMsg=%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);

			CThostFtdcUserLogoutField tn;
			memset(&tn, 0, sizeof(CThostFtdcUserLogoutField));
			memcpy_s(&tn, sizeof(CThostFtdcUserLogoutField), pUserLogout, sizeof(CThostFtdcUserLogoutField)); 
			EnterCriticalSection(&g_csdata);
			loginoutlist.push_back(tn);
			LeaveCriticalSection(&g_csdata);
			SetEvent(hEvent[EID_OnRspUserLoginout_Failer]);
		}
		else
		{
			printf("Scuess:OnRspUserLogout\n");
			CThostFtdcUserLogoutField tn;
			memset(&tn, 0, sizeof(CThostFtdcUserLogoutField));
			memcpy_s(&tn, sizeof(CThostFtdcUserLogoutField), pUserLogout, sizeof(CThostFtdcUserLogoutField));
			EnterCriticalSection(&g_csdata);
			loginoutlist.push_back(tn);
			LeaveCriticalSection(&g_csdata);
			SetEvent(hEvent[EID_OnRspUserLoginout_Scuess]);
		}
	}
}

void TraderSpi::ReqSettlementInfoConfirm()
{
	CThostFtdcSettlementInfoConfirmField req;
	memset(&req, 0, sizeof(CThostFtdcSettlementInfoConfirmField));
	strcpy_s(req.BrokerID,sizeof(req.BrokerID), gBrokerID.c_str());
	strcpy_s(req.InvestorID,sizeof(req.InvestorID), gUserID.c_str());
	int iResult=tdapi->ReqSettlementInfoConfirm(&req, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqSettlementInfoConfirm" <<  endl;
}

void TraderSpi::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (bIsLast)
	{
		::SetEvent(hSyncObj);
		CMDCONTENT tn;
		memset(&tn, 0, sizeof(CMDCONTENT));
		strncpy_s(tn.content, sizeof(tn.content), "确认结算单成功", sizeof("确认结算单成功"));
		tn.cmd = TD_SETTLEMENTINFOCONFIRM;
		EnterCriticalSection(&g_csdata);
		cmdlist.push_back(tn);
		LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_OnRspSettlementInfoConfirm]);
	}
}

bool TraderSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		SYSTEMTIME t;
		::GetLocalTime(&t);
		std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
		std::cout << "--->>> ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << std::endl;
		//::Beep(800, 10000);
		CThostFtdcRspInfoField tn;
		memset(&tn, 0, sizeof(CThostFtdcRspInfoField));
		tn.ErrorID = pRspInfo->ErrorID;
		strncpy_s(tn.ErrorMsg, sizeof(tn.ErrorMsg), pRspInfo->ErrorMsg, sizeof(pRspInfo->ErrorMsg));
		EnterCriticalSection(&g_csdata);
		errorlist.push_back(tn);
		LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_IsErrorRspInfo]);
	}
	return bResult;
}


void TraderSpi::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	IsErrorRspInfo(pRspInfo);
}

void TraderSpi::OnRtnOrder(CThostFtdcOrderField *pOrder)
{
	if (!pOrder)return;
	std::cout << __FUNCTION__ << std::endl;
		int orderRef = ::atoi(pOrder->OrderRef);
		::WaitForSingleObject(ghTradedVolMutex, INFINITE);
		gOrderRef2TradedVol[orderRef] = pOrder->VolumeTraded;
		::ReleaseMutex(ghTradedVolMutex);
	
        CThostFtdcOrderField tn;
		memset(&tn, 0, sizeof(CThostFtdcOrderField));
		memcpy_s(&tn,sizeof(tn), pOrder,sizeof(CThostFtdcOrderField));
		//strncpy_s(tn.content.BrokerID, sizeof(tn.content.BrokerID), pOrder->BrokerID, sizeof(pOrder->BrokerID));
		//strncpy_s(tn.content.InvestorID, sizeof(tn.content.InvestorID), pOrder->InvestorID, sizeof(pOrder->InvestorID));
		//strncpy_s(tn.content.InstrumentID, sizeof(tn.content.InstrumentID), pOrder->InstrumentID, sizeof(pOrder->InstrumentID));
		//strncpy_s(tn.content.OrderRef, sizeof(tn.content.OrderRef), pOrder->OrderRef, sizeof(pOrder->OrderRef));

		//strncpy_s(tn.content.UserID, sizeof(tn.content.UserID), pOrder->UserID, sizeof(pOrder->UserID));
		//strncpy_s(tn.content.InvestorID, sizeof(tn.content.InvestorID), pOrder->InvestorID, sizeof(pOrder->InvestorID));
		//strncpy_s(tn.content.InstrumentID, sizeof(tn.content.InstrumentID), pOrder->InstrumentID, sizeof(pOrder->InstrumentID));
		//strncpy_s(tn.content.OrderRef, sizeof(tn.content.OrderRef), pOrder->OrderRef, sizeof(pOrder->OrderRef));
		//tn.cmd = TD_ORDER_INFO;
		EnterCriticalSection(&g_csdata);
		orderlist.push_back(tn);
		LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_OnRspOrder]);
        /*
		ordercontent tn;
		memset(&tn, 0, sizeof(ordercontent));
		//(tn.content, sizeof(tn.content), pRspInfo->ErrorMsg, sizeof(pRspInfo->ErrorMsg));
		tn.cmd = TD_ORDER_INFO;
		EnterCriticalSection(&g_csdata);
		orderlist.push_back(tn);
		LeaveCriticalSection(&g_csdata);
		//LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_OnRspOrder]);
		*/
	//}
}

void TraderSpi::OnRtnTrade(CThostFtdcTradeField *pTrade)
{
	if (!pTrade)return;
	std::cout << __FUNCTION__ << std::endl;
	//int orderRef = ::atoi(pTrade->OrderRef);
	/*
	::WaitForSingleObject(ghTradedVolMutex, INFINITE);
	gOrderRef2TradedVol[orderRef] = pTrade->VolumeTraded;
	::ReleaseMutex(ghTradedVolMutex);
	*/
	///成交
	CThostFtdcTradeField tn;
	memset(&tn, 0, sizeof(CThostFtdcTradeField));
	memcpy_s(&tn, sizeof(CThostFtdcTradeField), pTrade, sizeof(CThostFtdcTradeField));
	//strncpy_s(tn.content.BrokerID, sizeof(tn.content.BrokerID), pOrder->BrokerID, sizeof(pOrder->BrokerID));
	//strncpy_s(tn.content.InvestorID, sizeof(tn.content.InvestorID), pOrder->InvestorID, sizeof(pOrder->InvestorID));
	//strncpy_s(tn.content.InstrumentID, sizeof(tn.content.InstrumentID), pOrder->InstrumentID, sizeof(pOrder->InstrumentID));
	//strncpy_s(tn.content.OrderRef, sizeof(tn.content.OrderRef), pOrder->OrderRef, sizeof(pOrder->OrderRef));
	//strncpy_s(tn.content.UserID, sizeof(tn.content.UserID), pOrder->UserID, sizeof(pOrder->UserID));
	//strncpy_s(tn.content.InvestorID, sizeof(tn.content.InvestorID), pOrder->InvestorID, sizeof(pOrder->InvestorID));
	//strncpy_s(tn.content.InstrumentID, sizeof(tn.content.InstrumentID), pOrder->InstrumentID, sizeof(pOrder->InstrumentID));
	//strncpy_s(tn.content.OrderRef, sizeof(tn.content.OrderRef), pOrder->OrderRef, sizeof(pOrder->OrderRef));
	EnterCriticalSection(&g_csdata);
	tradelist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRspTrade]);

}

//合约交易状态通知
 
void  TraderSpi::OnRtnInstrumentStatus(CThostFtdcInstrumentStatusField *pInstrumentStatus)
{
	if (!pInstrumentStatus)return;
	CThostFtdcInstrumentStatusField tn;
	memset(&tn, 0, sizeof(CThostFtdcInstrumentStatusField));
	memcpy_s(&tn, sizeof(CThostFtdcInstrumentStatusField), pInstrumentStatus, sizeof(CThostFtdcInstrumentStatusField));
	//strncpy_s(tn.content.BrokerID, sizeof(tn.content.BrokerID), pOrder->BrokerID, sizeof(pOrder->BrokerID));
	//strncpy_s(tn.content.InvestorID, sizeof(tn.content.InvestorID), pOrder->InvestorID, sizeof(pOrder->InvestorID));
	//strncpy_s(tn.content.InstrumentID, sizeof(tn.content.InstrumentID), pOrder->InstrumentID, sizeof(pOrder->InstrumentID));
	//strncpy_s(tn.content.OrderRef, sizeof(tn.content.OrderRef), pOrder->OrderRef, sizeof(pOrder->OrderRef));
	//strncpy_s(tn.content.UserID, sizeof(tn.content.UserID), pOrder->UserID, sizeof(pOrder->UserID));
	//strncpy_s(tn.content.InvestorID, sizeof(tn.content.InvestorID), pOrder->InvestorID, sizeof(pOrder->InvestorID));
	//strncpy_s(tn.content.InstrumentID, sizeof(tn.content.InstrumentID), pOrder->InstrumentID, sizeof(pOrder->InstrumentID));
	//strncpy_s(tn.content.OrderRef, sizeof(tn.content.OrderRef), pOrder->OrderRef, sizeof(pOrder->OrderRef));
	EnterCriticalSection(&g_csdata);
	InstrumentStatuslist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRtnInstrumentStatus]);
}

int TraderSpi::DeleteOrder(char *InstrumentID, DWORD orderRef)
{
	if (!InstrumentID)return -1;
	std::cout << __FUNCTION__ << std::endl;
	CThostFtdcInputOrderActionField ReqDel;
	::ZeroMemory(&ReqDel, sizeof(ReqDel));
	_snprintf_s(ReqDel.BrokerID,sizeof(ReqDel.BrokerID),  sizeof(ReqDel.BrokerID)-1,"%s", gBrokerID.c_str());
	_snprintf_s(ReqDel.InvestorID, sizeof(ReqDel.InvestorID),  sizeof(ReqDel.BrokerID) - 1, "%s", gUserID.c_str());
	_snprintf_s(ReqDel.InstrumentID, sizeof(ReqDel.InstrumentID), sizeof(ReqDel.BrokerID) - 1, "%s", InstrumentID);
	_snprintf_s(ReqDel.OrderRef, sizeof(ReqDel.OrderRef), sizeof(ReqDel.OrderRef) - 1, "%012d", orderRef);
	ReqDel.FrontID = FRONT_ID;
	ReqDel.SessionID = SESSION_ID;
	ReqDel.ActionFlag = THOST_FTDC_AF_Delete;
	int iResult = tdapi->ReqOrderAction(&ReqDel, ++(iRequestID));
	if (iResult != 0)
		cerr << "Failer: DeleteOrder" <<  endl;
	return iResult;
}

VOID MakeOrder(CThostFtdcInputOrderField *pOrder)
{
	memset(pOrder, 0, sizeof(*pOrder));
	_snprintf_s(pOrder->BrokerID,sizeof(pOrder->BrokerID), sizeof(pOrder->BrokerID)-1,"%s", gBrokerID.c_str());
	_snprintf_s(pOrder->InvestorID, sizeof(pOrder->InvestorID), sizeof(pOrder->BrokerID) - 1, "%s", gUserID.c_str());

	///用户代码
	//TThostFtdcUserIDType	UserID;
	///组合投机套保标志
	pOrder->CombHedgeFlag[0] = THOST_FTDC_HF_Speculation;	
	///限价单
	pOrder->OrderPriceType = THOST_FTDC_OPT_LimitPrice;
	///触发条件: 立即
	pOrder->ContingentCondition = THOST_FTDC_CC_Immediately;
	///有效期类型: 当日有效
	pOrder->TimeCondition = THOST_FTDC_TC_GFD;
	///GTD日期
	//	TThostFtdcDateType	GTDDate;
	///成交量类型: 任何数量
	pOrder->VolumeCondition = THOST_FTDC_VC_AV;
	///最小成交量: 1
	pOrder->MinVolume = 1;
	///止损价
	//	TThostFtdcPriceType	StopPrice;
	///强平原因: 非强平
	pOrder->ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
	///自动挂起标志: 否
	pOrder->IsAutoSuspend = 0;
	///业务单元
	//	TThostFtdcBusinessUnitType	BusinessUnit;
	///请求编号
	//	TThostFtdcRequestIDType	RequestID;
	///用户强评标志: 否
	pOrder->UserForceClose = 0;
}

int TraderSpi:: InsertOrder(char *InstrumentID, char dir,char offsetFlag, char priceType, double price, int num)
{
	CThostFtdcInputOrderField req;
	MakeOrder(&req);
	_snprintf_s(req.InstrumentID, sizeof(req.InstrumentID), sizeof(req.InstrumentID) - 1, "%s", InstrumentID);
	req.CombOffsetFlag[0] = offsetFlag;
	req.Direction = dir;
	req.VolumeTotalOriginal = num;
	req.LimitPrice = price;
	req.OrderPriceType = priceType;
	++(iOrderRef);
	_snprintf_s(req.OrderRef,sizeof(req.OrderRef), sizeof(req.OrderRef)-1, "%012d", iOrderRef);
	int iResult = tdapi->ReqOrderInsert(&req, ++(iRequestID));
	if (iResult != 0)
		cerr << "Failer: InsertOrder" <<   endl;
	return iOrderRef;
}

void TraderSpi::ReqQryInvestorPosition()
{
	if (tdapi == NULL)return;
	CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(CThostFtdcQryInvestorPositionField));
	_snprintf_s(req.BrokerID, sizeof(req.BrokerID), sizeof(req.BrokerID) - 1, "%s", gBrokerID.c_str());
	_snprintf_s(req.InvestorID, sizeof(req.InvestorID), sizeof(req.InvestorID) - 1, "%s", gUserID.c_str());
	int iResult = tdapi->ReqQryInvestorPosition(&req, ++iRequestID);
	//cerr << "--->>> 请求查询投资者持仓: " << ((iResult == 0) ? "成功" : "失败") << endl;
	if (iResult != 0)
		cerr << "Failer: ReqQryInvestorPosition " <<  endl;
 
}

///请求查询投资者品种/跨品种保证金
void TraderSpi::ReqQryInvestorProductGroupMargin(char *Instrument)
{
	if (tdapi == NULL)return;
	//CThostFtdcQryInvestorPositionField req = { 0 };
	//strcpy(req.BrokerID, m_BrokerID);
	//strcpy(req.InvestorID, m_InvestorInfos[reqInfo.lAccIdx].InvestorID);
	//req.InstrumentID; //指定合约的话，就是查询特定合约的持仓信息，不填就是查询所有持仓  
	//ReqQryInvestorPosition(&req, reqInfo.nRequestID);
	//CThostFtdcQryInvestorProductGroupMarginField
	CThostFtdcQryInvestorProductGroupMarginField req;
	//CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(CThostFtdcQryInvestorProductGroupMarginField));
	_snprintf_s(req.BrokerID, sizeof(req.BrokerID), sizeof(req.BrokerID) - 1, "%s", gBrokerID.c_str());
	_snprintf_s(req.InvestorID,sizeof(req.InvestorID), sizeof(req.InvestorID)-1,"%s", gUserID.c_str());
	req.HedgeFlag = '1';

	//printf("查询保证金[%s]   [%s]  [%s]", req.BrokerID, req.InvestorID);
	//1>CTPTraderSpi.cpp(654) : error C2664 : “int CThostFtdcTraderApi::ReqQryInstrumentMarginRate(
	//CThostFtdcQryInstrumentMarginRateField *, int)” : 无法将参数 1 从“
	//CThostFtdcInstrumentMarginRateField *”转换为“CThostFtdcQryInstrumentMarginRateField *”
	//strcpy(req.InstrumentID, INSTRUMENT_ID);
	//printf("指定持仓%s", INSTRUMENT_ID);
	int iResult = tdapi->ReqQryInvestorProductGroupMargin(&req, ++iRequestID);
	//cerr << "--->>> 请求查询投资者持仓: " << ((iResult == 0) ? "成功" : "失败") << endl;
	if (iResult != 0)
		cerr << "Failer: ReqQryInvestorProductGroupMargin" <<   endl;
 
}

int TraderSpi::ReqQryMaxOrderVolume(CThostFtdcQryMaxOrderVolumeField *pQueryMaxOrderVolume, int nRequestID)
{
	int iResult = tdapi->ReqQryMaxOrderVolume(pQueryMaxOrderVolume, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqQryMaxOrderVolume " <<  endl;
	return iResult;
}

///期货发起银行资金转期货请求
int TraderSpi::ReqFromBankToFutureByFuture(CThostFtdcReqTransferField *pReqTransfer, int nRequestID)
{
	int iResult = tdapi->ReqFromBankToFutureByFuture(pReqTransfer, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqFromBankToFutureByFuture: " <<  endl;
	return iResult;
}

///期货发起期货资金转银行请求
int TraderSpi::ReqFromFutureToBankByFuture(CThostFtdcReqTransferField *pReqTransfer, int nRequestID)
{
	int iResult = tdapi->ReqFromFutureToBankByFuture(pReqTransfer, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqFromFutureToBankByFuture" <<  endl;
	return iResult;
}


void TraderSpi::ReqQryInstrument(char *Instrument)
{
	if (tdapi == NULL)return;
	CThostFtdcQryInstrumentField req;
	memset(&req, 0, sizeof(CThostFtdcQryInstrumentField));
	_snprintf_s(req.ExchangeID, sizeof(req.ExchangeID), sizeof(req.ExchangeID) - 1, "%s", "DCE");
	_snprintf_s(req.ExchangeInstID, sizeof(req.ExchangeInstID), sizeof(req.ExchangeInstID) - 1, "");
	_snprintf_s(req.ProductID, sizeof(req.ProductID), sizeof(req.ProductID) - 1, "");
	_snprintf_s(req.InstrumentID, sizeof(req.InstrumentID), sizeof(req.InstrumentID) - 1, "%s", Instrument);
	//printf("查询乘数[%s]   [%s]  [%s]", req.ExchangeID, req.ProductID, req.InstrumentID);
	int iResult = tdapi->ReqQryInstrument(&req, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqQryInstrument" << endl;
}
///请求查询合约保证金率
void TraderSpi::ReqQryInstrumentMarginRate(char *Instrument)
{
	if (tdapi == NULL)return;
	//CThostFtdcQryInvestorPositionField req = { 0 };
	//strcpy(req.BrokerID, m_BrokerID);
	//strcpy(req.InvestorID, m_InvestorInfos[reqInfo.lAccIdx].InvestorID);
	//req.InstrumentID; //指定合约的话，就是查询特定合约的持仓信息，不填就是查询所有持仓  
	//ReqQryInvestorPosition(&req, reqInfo.nRequestID);
	CThostFtdcQryInstrumentMarginRateField req;
	memset(&req, 0, sizeof(CThostFtdcQryInstrumentMarginRateField));
	_snprintf_s(req.BrokerID,sizeof(req.BrokerID),   sizeof(req.BrokerID)-1,"%s", gBrokerID.c_str());
	_snprintf_s(req.InvestorID, sizeof(req.InvestorID), sizeof(req.BrokerID) - 1, "%s", gUserID.c_str());
	_snprintf_s(req.InstrumentID, sizeof(req.InstrumentID), sizeof(req.BrokerID) - 1, "%s", Instrument);
	req.HedgeFlag = '1';
	//printf("查询保证金[%s] 投资者账户[%s]合约[%s]\n",req.BrokerID, req.InvestorID, req.InstrumentID);
	//1>CTPTraderSpi.cpp(654) : error C2664 : “int CThostFtdcTraderApi::ReqQryInstrumentMarginRate(
		//CThostFtdcQryInstrumentMarginRateField *, int)” : 无法将参数 1 从“
		//CThostFtdcInstrumentMarginRateField *”转换为“CThostFtdcQryInstrumentMarginRateField *”
	//printf("\n----------------------------------------\n");
	//strcpy(req.InstrumentID, INSTRUMENT_ID);
	//printf("指定持仓%s", INSTRUMENT_ID);
	int iResult = tdapi->ReqQryInstrumentMarginRate(&req, ++iRequestID);
	//cerr << "--->>> 请求查询投资者持仓: " << ((iResult == 0) ? "成功" : "失败") << endl;
	if (iResult != 0)
		cerr << "Failer: ReqQryInstrumentMarginRate" << endl;
}



///请求查询合约
int TraderSpi::ReqQryInstrument(CThostFtdcQryInstrumentField *pQryInstrument, int nRequestID)
{
	if (pQryInstrument == NULL)	return 0;
	CThostFtdcQryInstrumentField req;
	memset(&req, 0, sizeof(CThostFtdcQryInstrumentField));
	//strcpy(req.BrokerID, gBrokerID.c_str());
	//strcpy(req.InvestorID, gUserID.c_str());
	//strcpy(req.InstrumentID, Instrument);
	int iResult = tdapi->ReqQryInstrument(&req, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqQryInstrument" << endl;
	return iResult;
}

int TraderSpi::ReqQryContractBank(CThostFtdcQryContractBankField *pQryContractBank, int nRequestID)
{
	if (pQryContractBank == NULL)return -1;
	CThostFtdcQryContractBankField req;
	memset(&req, 0, sizeof(CThostFtdcQryContractBankField));
	_snprintf_s(req.BrokerID, sizeof(req.BrokerID), sizeof(req.BrokerID)-1,"%s", gBrokerID.c_str());
	int iResult = tdapi->ReqQryContractBank(&req, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqQryContractBank" << endl;
	return iResult;
}

void TraderSpi::ReqQryTradingAccount()
{
	if (tdapi == NULL)return;
	CThostFtdcQryTradingAccountField req;
	memset(&req, 0, sizeof(CThostFtdcQryTradingAccountField));
	_snprintf_s(req.BrokerID, sizeof(req.BrokerID), sizeof(req.BrokerID) - 1,       "%s", gBrokerID.c_str());
	_snprintf_s(req.InvestorID, sizeof(req.InvestorID), sizeof(req.InvestorID) - 1, "%s", gUserID.c_str());

	int iResult = tdapi->ReqQryTradingAccount(&req, ++iRequestID);
	if (iResult != 0)
		cerr << "Failer: ReqQryTradingAccount" << endl;
}


void TraderSpi::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pInvestorPosition == NULL)return;
 	while (nRequestID != SaveInstrumentID)
	{
		SaveInstrumentID = nRequestID;
		gTypeCheckState_B_History[pInvestorPosition->InstrumentID] = false;
		gTypeCheckState_B_Today[pInvestorPosition->InstrumentID] = false;
		gTypeCheckState_S_History[pInvestorPosition->InstrumentID] = false;
		gTypeCheckState_S_Today[pInvestorPosition->InstrumentID] = false;
	}

	if (pInvestorPosition->PosiDirection == '2' &&  pInvestorPosition->Position != 0) //注意别的版本要修复
			{
				if (pInvestorPosition->TodayPosition == 0) //今仓
				{
					//TypeCheckState_B_History[i] = true;
					gTypeCheckState_B_History[pInvestorPosition->InstrumentID] = true;
					//Trade_dataA_Amount_B_History[i] = pInvestorPosition->Position;

					//持仓map
					gPosition_B_History[pInvestorPosition->InstrumentID] = pInvestorPosition->Position;

					//printf("历史买单 [%d]\n", gPosition_B_History[pInvestorPosition->InstrumentID]);

				}
				else
				{
					//TypeCheckState_B_Today[i] = true;
					gTypeCheckState_B_Today[pInvestorPosition->InstrumentID] = true;
					//Trade_dataA_Amount_B_Today[i] = pInvestorPosition->Position;

					//持仓map
					gPosition_B_Today[pInvestorPosition->InstrumentID] = pInvestorPosition->Position;

					//printf("今仓买单 [%d]\n", gPosition_B_Today[pInvestorPosition->InstrumentID]);
				}
			}
	else if (pInvestorPosition->PosiDirection == '3' &&  pInvestorPosition->Position != 0)  //注意别的版本要修复
			{
				if (pInvestorPosition->TodayPosition == 0) //今仓
				{
					//TypeCheckState_S_History[i] = true;
					gTypeCheckState_S_History[pInvestorPosition->InstrumentID] = true;
					//Trade_dataA_Amount_S_History[i] = pInvestorPosition->Position;

					//持仓map
					gPosition_S_History[pInvestorPosition->InstrumentID] = (pInvestorPosition->Position);

					//printf("历史卖单 [%d]\n", gPosition_S_History[pInvestorPosition->InstrumentID]);
				}
				else
				{
					//TypeCheckState_S_Today[i] = true;
					gTypeCheckState_S_Today[pInvestorPosition->InstrumentID] = true;
					//Trade_dataA_Amount_S_Today[i] = pInvestorPosition->Position;

					//持仓map
					gPosition_S_Today[pInvestorPosition->InstrumentID] = (pInvestorPosition->Position);

					//printf("今仓卖单 [%d]\n", gPosition_S_Today[pInvestorPosition->InstrumentID]);
				}
			}

	if (bIsLast)
	{
		for (int i = 0; i < 20; i++)
		{
			if(!gTypeCheckState_B_History[pInvestorPosition->InstrumentID])
			{
				gPosition_B_History[pInvestorPosition->InstrumentID] = 0;
			}
			if(!gTypeCheckState_B_Today[pInvestorPosition->InstrumentID])
			{
				//持仓map
				gPosition_B_Today[pInvestorPosition->InstrumentID] = 0;
			}

			if(!gTypeCheckState_S_History[pInvestorPosition->InstrumentID])
			{
				//持仓map
				gPosition_S_History[pInvestorPosition->InstrumentID] = 0;
			}

			if(!gTypeCheckState_S_Today[pInvestorPosition->InstrumentID])
			{
				//持仓map
				gPosition_S_Today[pInvestorPosition->InstrumentID] = 0;
			}
			//	printf("品种：%s  BUY持仓(今仓)[%d] BUY持仓(历史仓)[%d] SELL持仓(今仓)[%d] SELL持仓(历史)[%d]\n", InstrumentID_n[i], Trade_dataA_Amount_B_Today[i], Trade_dataA_Amount_B_History[i], Trade_dataA_Amount_S_Today[i], Trade_dataA_Amount_S_History[i]);
		}
	}

	gPosition_S[pInvestorPosition->InstrumentID] = gPosition_S_Today[pInvestorPosition->InstrumentID] + gPosition_S_History[pInvestorPosition->InstrumentID];
	gPosition_B[pInvestorPosition->InstrumentID] = gPosition_B_Today[pInvestorPosition->InstrumentID] + gPosition_B_History[pInvestorPosition->InstrumentID];
	if (showpositionstate)
	{
		printf("%s ：总卖单[%d] 今卖单[%d] 非今日卖单[%d] 总买单[%d] 今买单[%d] 非今日买单[%d]\n", pInvestorPosition->InstrumentID,
		gPosition_S[pInvestorPosition->InstrumentID], gPosition_S_Today[pInvestorPosition->InstrumentID], gPosition_S_History[pInvestorPosition->InstrumentID],
		gPosition_B[pInvestorPosition->InstrumentID], gPosition_B_Today[pInvestorPosition->InstrumentID] , gPosition_B_History[pInvestorPosition->InstrumentID]);
	}
}

void TraderSpi::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pTradingAccount == NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		//cerr << "--->>> 交易日: " << pTradingAccount->TradingDay << "\n" << endl;
		//cerr << "--->>> \n可用资金: " << (int)(pTradingAccount->Available / 10000) << "万\n" << endl;
		//cerr << "--->>> 可取资金: " << pTradingAccount->WithdrawQuota  << endl;
		//静态权益=上日结算-出金金额+入金金额
		double preBalance = pTradingAccount->PreBalance - pTradingAccount->Withdraw + pTradingAccount->Deposit;
		//cerr << "--->>> 静态权益: " << preBalance  << endl;
		//动态权益=静态权益+ 平仓盈亏+ 持仓盈亏- 手续费
		double current = preBalance + pTradingAccount->CloseProfit + pTradingAccount->PositionProfit - pTradingAccount->Commission;
		//cerr << "--->>> 动态权益: " << current  << endl;
		YestayAllAmount = preBalance; //静态权益
		TodayAllAmount = current; //动态权益
		UserAmount = pTradingAccount->Available;  //可用资金
	}
}

void TraderSpi::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{	
	if (pInstrument == NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{	//	CThostFtdcInstrumentField tn;
		//memset(&tn, 0, sizeof(CThostFtdcInstrumentField));
		//memcpy_s(&tn, sizeof(CThostFtdcInstrumentField), pInstrument, sizeof(CThostFtdcInstrumentField));
       gUnderlyingMultiple[pInstrument->InstrumentID] =  pInstrument->UnderlyingMultiple;
	}
}

 

void TraderSpi::OnRspQryInvestorProductGroupMargin(CThostFtdcInvestorProductGroupMarginField *pInvestorProductGroupMargin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pInvestorProductGroupMargin == NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
	
	}
}

void TraderSpi::OnRspQryInstrumentMarginRate(CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pInstrumentMarginRate == NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		 gMarginRate_long[pInstrumentMarginRate->InstrumentID] = pInstrumentMarginRate->LongMarginRatioByMoney;// LongMarginRatioByMoney;// gPosition_S_Today[pInstrumentMarginRate->InstrumentID] + gPosition_S_History[pInstrumentMarginRate->InstrumentID];
		 gMarginRate_short[pInstrumentMarginRate->InstrumentID] = pInstrumentMarginRate->ShortMarginRatioByMoney;// gPosition_S_Today[pInstrumentMarginRate->InstrumentID] + gPosition_S_History[pInstrumentMarginRate->InstrumentID];
	}
}

void TraderSpi::OnRspQryInstrumentCommissionRate(CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pInstrumentCommissionRate == NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		CThostFtdcInstrumentCommissionRateField tn;
		memset(&tn, 0, sizeof(CThostFtdcInstrumentCommissionRateField));
		memcpy_s(&tn, sizeof(tn), pInstrumentCommissionRate, sizeof(CThostFtdcInstrumentCommissionRateField));

		pInstrumentCommissionRate->BrokerID;
		pInstrumentCommissionRate->CloseRatioByMoney;
		pInstrumentCommissionRate->CloseRatioByVolume;
		pInstrumentCommissionRate->CloseTodayRatioByMoney;
		pInstrumentCommissionRate->CloseTodayRatioByVolume;
		pInstrumentCommissionRate->InstrumentID;
		pInstrumentCommissionRate->InvestorID;
		pInstrumentCommissionRate->InvestorRange;
		pInstrumentCommissionRate->OpenRatioByMoney;
	}
}
                     

///查询最大报单数量响应
void TraderSpi::OnRspQryMaxOrderVolume(CThostFtdcQryMaxOrderVolumeField *pQueryMaxOrderVolume, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pQueryMaxOrderVolume == NULL)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		CThostFtdcQryMaxOrderVolumeField tn;
		memset(&tn, 0, sizeof(CThostFtdcQryMaxOrderVolumeField));
		memcpy_s(&tn, sizeof(tn), pQueryMaxOrderVolume, sizeof(CThostFtdcQryMaxOrderVolumeField));
		EnterCriticalSection(&g_csdata);
		querymaxordervolumelist.push_back(tn);
		LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_OnRspQueryMaxOrderVolume]);
	}
}

void TraderSpi::OnRspQryAccountregister(CThostFtdcAccountregisterField *pAccountregister, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pAccountregister == NULL)return;
	CThostFtdcAccountregisterField tn;
	memset(&tn, 0, sizeof(CThostFtdcAccountregisterField));
	memcpy_s(&tn, sizeof(tn), pAccountregister, sizeof(CThostFtdcAccountregisterField));
	EnterCriticalSection(&g_csdata);
	qryaccountregisterlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRspQryAccountregister]);
}

///期货发起银行资金转期货通知
void TraderSpi::OnRtnFromBankToFutureByFuture(CThostFtdcRspTransferField *pRspTransfer)
{
	if (pRspTransfer == NULL)return;
	CThostFtdcRspTransferField tn;
	memset(&tn, 0, sizeof(CThostFtdcRspTransferField));
	memcpy_s(&tn, sizeof(tn), pRspTransfer, sizeof(CThostFtdcRspTransferField));
	EnterCriticalSection(&g_csdata);
	banktofuturebyfuturelist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRtnFromBankToFutureByFuture]);
}


///期货发起期货资金转银行通知
void TraderSpi::OnRtnFromFutureToBankByFuture(CThostFtdcRspTransferField *pRspTransfer)
{
	if (pRspTransfer == NULL)return;
	CThostFtdcRspTransferField tn;
	memset(&tn, 0, sizeof(CThostFtdcRspTransferField));
	memcpy_s(&tn, sizeof(tn), pRspTransfer, sizeof(CThostFtdcRspTransferField));
	EnterCriticalSection(&g_csdata);
	futuretobankbyfuturelist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRtnFromFutureToBankByFuture]);
}

