/*
（1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
适合学习入门和拓展开发，就在1.0版发布后不久，我的vnapi 1.0（当时不叫vnapi）就遭受了非法金融经营者用1.2万字连载诽谤文的诋毁攻击，
他们甚至在诽谤文下方还放上他们自己的涉及非法金融开源软件的广告。
从那次诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？
当初遇此情形，我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
写出了virtualapi仿真回测（盛立版、CTP版、通联Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆。
由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
其产品甚至短暂的上了某期货公司官网。
在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，我忠心的希望金融从业者们牢记合规底线，
勿给非法金融可乘之机。
本次发布的vnapi2.10 期货CTP接口行情库底层代码，源于2017年4月的1.0开源版本，已更新至支持新版的CTP接口。
改名前的2.09 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
这6年时间，我们一边开发产品，一边开始了长达6年的维权之路。
vnapi2.0作为一款Python CTP接口的框架，基于Ctypes技术，简单易学，主要目的是为了为开发者提供一个通俗易懂
的后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。
本团队还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn），
面向的是专业机构客户，VNTrader系列均是在vnapi 1.0后端框架技术上迭代产生的，
当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于quant林和上海量贝软件的原创产品，
quant林于2014年获得基于AR增强现实技术的国家发明专利一项。
quant林另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。

（2）vnapi 遵循 开源协议GPL v3
简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
官方网站：http://www.vnpy.cn
*/
#pragma once

#	define VNAPI_TD_API __declspec(dllexport)

#ifdef __cplusplus
extern "C" {
#endif

	/*----------以下是对CTP交易API头文件方法的调用实现--------*/
	///用户登录请求
	VNAPI_TD_API int     ReqUserLogin();
	VNAPI_TD_API int     InsertOrderByRate(char *Instrument, char direction, char offsetFlag, char priceType, double price,double rate, bool BalanceType,int multiplier);
	VNAPI_TD_API int     InsertOrder(char *Instrument,char direction, char offsetFlag, char priceType, double price, int num);
	VNAPI_TD_API int     DeleteOrder(char *Instrument, int OrderRef);
	VNAPI_TD_API int     QryTradedVol(int OrderRef);
	VNAPI_TD_API int     QryPosition(char *Instrument,int positiontype);  
	VNAPI_TD_API int     ReqQryMaxOrderVolume(char * BrokerID, char * InvestorID, char * InstrumentID, char Direction, char  OffsetFlag, char  HedgeFlag, int MaxVolume);
	VNAPI_TD_API int     ReqQryContractBank();
	VNAPI_TD_API double  ReqQryInstrumentMarginRate(char *Instrument, int positiontype);
	VNAPI_TD_API double   QryUnderlyingMultiple(char *Instrument);  
	VNAPI_TD_API int ReqFromBankToFutureByFuture(char * BankID,char *  BrokerBranchID,char *BankAccount,char * BankPassWord,
	char * AccountID,double  TradeAmount,int nRequestID);
    ///期货发起期货资金转银行请求
	VNAPI_TD_API int    ReqFromFutureToBankByFuture(char * BankID,char *  BrokerBranchID,char *BankAccount,char * BankPassWord,char * AccountID,double  TradeAmount,int nRequestID);
	
	/*----------以下是对CTP SPI回调事件队列处理--------*/
	///获得回调指令类型
	///@return  
	VNAPI_TD_API int     GetCmd();
	///获得具体的回调指令内容
	///@return  CMDCONTENT
	VNAPI_TD_API void  * GetCmdContent();
	///定义的CTP回调函数转Python回调指令
	///@retrun   int  表明是何种回调类型
	VNAPI_TD_API int  OnCmd();
	///获取回调对列大小
	///@retrun   int   返回回调对列大小
	VNAPI_TD_API int  GetUnGetCmdSize();
	///获取错误应答消息
	///@retrun 错误应答CThostFtdcRspInfoField
	VNAPI_TD_API void  * GetCmdContent_Error();
	///获取连接成功消息，当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	VNAPI_TD_API void  * GetCmdContent_Connected();
	///获取登录成功
	///@retrun   CThostFtdcRspUserLoginField
	VNAPI_TD_API void *  GetCmdContent_LoginScuess();
	/// 结算确认消息
	///@return CThostFtdcSettlementInfoConfirmField 
	VNAPI_TD_API void  * GetCmdContent_Settlement();
	///获得委托回调消息
	///@return  CThostFtdcOrderField
	VNAPI_TD_API void  * GetCmdContent_Order();
	///获得成交回报消息
	///@return  CThostFtdcTradeField
	VNAPI_TD_API void  * GetCmdContent_Trade();
	///获得期货发起银行资金转期货通知消息
	///@return   CThostFtdcRspTransferField
	VNAPI_TD_API void  * GetCmdContent_BankToFutureByFuture();
	///期货发起银行资金转期货通知消息
	///@return   CThostFtdcRspTransferField
	VNAPI_TD_API void  * GetCmdContent_FutureToBankByFuture();
	/// 查询最大报单数量响应消息
	///@return    CThostFtdcQryMaxOrderVolumeField
	VNAPI_TD_API void  * GetCmdContent_QueryMaxOrderVolume();
	VNAPI_TD_API void  * GetCmdContent_ProductGroupMargin();
	VNAPI_TD_API void  * GetCmdContent_CommissionRate();
	VNAPI_TD_API void  * GetCmdContent_BankToFutureByFuture();
	VNAPI_TD_API void  * GetCmdContent_FutureToBankByFuture();
 
	/*----------以下是vnapi其他方法--------*/
	VNAPI_TD_API int  ReqQryInstrument();
	VNAPI_TD_API void  *QryPositionList(int i);
	VNAPI_TD_API double  QryBalance(bool BalanceType);     //1静态权益 0动态权益 自己增加
	VNAPI_TD_API double  QryAvailable();   //可用资金
	VNAPI_TD_API void  SetShowPosition(bool showstate);   //可用资金


//double VNAPI_TD_API QryAvailable2();   //可用资金
#ifdef __cplusplus
}
#endif