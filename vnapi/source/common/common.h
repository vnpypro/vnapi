#include "../common/ctpapi/ThostTraderApiX64/ThostFtdcUserApiDataType.h"
#include "../common/ctpapi/ThostTraderApiX64/ThostFtdcUserApiStruct.h"
#include "../common/ctpapi/ThostTraderApiX64/ThostFtdcTraderApi.h"
#include "../common/ctpapi/ThostTraderApiX64/ThostFtdcMdApi.h"

#if !defined(COMMON_H)
#define COMMON_H
typedef void *HANDLE;
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif
struct CMDCONTENT
{
	int cmd;
	int reason;
	char content[31];
};

//MAX_EVENTNUM取可以设置回调事件种类的最大值64，最大定义64个指令，不得更改为大于64的值
#define MAX_EVENTNUM  64

extern HANDLE hEvent[MAX_EVENTNUM];

#endif
