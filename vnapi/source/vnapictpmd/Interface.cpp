/*
（1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
适合学习入门和拓展开发，就在1.0版发布后不久，我的vnapi 1.0（当时不叫vnapi）就遭受了非法金融经营者用1.2万字连载诽谤文的诋毁攻击，
他们甚至在诽谤文下方还放上他们自己的涉及非法金融开源软件的广告。
从那次诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？
当初遇此情形，我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
写出了virtualapi仿真回测（盛立版、CTP版、通联Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆。
由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
其产品甚至短暂的上了某期货公司官网。
在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，我忠心的希望金融从业者们牢记合规底线，
勿给非法金融可乘之机。
本次发布的vnapi2.10 期货CTP接口行情库底层代码，源于2017年4月的1.0开源版本，已更新至支持新版的CTP接口。
改名前的2.09 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
这6年时间，我们一边开发产品，一边开始了长达6年的维权之路。
vnapi2.0作为一款Python CTP接口的框架，基于Ctypes技术，简单易学，主要目的是为了为开发者提供一个通俗易懂
的后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。
本团队还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn），
面向的是专业机构客户，VNTrader系列均是在vnapi 1.0后端框架技术上迭代产生的，
当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于quant林和上海量贝软件的原创产品，
quant林于2014年获得基于AR增强现实技术的国家发明专利一项。
quant林另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。

（2）vnapi 遵循 开源协议GPL v3
简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
官方网站：http://www.vnpy.cn
*/
#pragma once
#include "stdafx.h"
#include "../common/common.h"
#include "def.h"
#include "Interface.h"
#include "MdSpi.h"
#include <map>
#include <string>
#include <algorithm>
#include "iostream"
#include <iostream> 
#include <windows.h>
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <vector>
#include <string>
#include <iosfwd>
#include <conio.h>
#include <ctype.h>
using std::vector;
using std::string;
using std::wstring;
using std::iterator;
using namespace std;



extern int savetickstate;
extern CRITICAL_SECTION g_csdata;
list <CMDCONTENT> cmdlist;
list <CMDCONTENT>::iterator cmd_Iter;
list <TThostFtdcInstrumentIDTypeStruct> ticknamelist;
list <TThostFtdcInstrumentIDTypeStruct>::iterator tickname_Iter;
list <CThostFtdcRspInfoField> errorlist;
list <CThostFtdcRspInfoField>::iterator error_Iter;
list <CThostFtdcRspUserLoginField> loginlist;
list <CThostFtdcRspUserLoginField>::iterator login_Iter;
list <CThostFtdcRspUserLoginField> loginfailelist;
list <CThostFtdcRspUserLoginField>::iterator loginfaile_Iter;
list <CThostFtdcUserLogoutField> loginoutlist;
list <CThostFtdcUserLogoutField>::iterator loginout_Iter;
list <int> connectlist;
list <int>::iterator connect_Iter;
list <CThostFtdcSpecificInstrumentField> subMarketlist;
list <CThostFtdcSpecificInstrumentField>::iterator subMarket_Iter;
list <CThostFtdcSpecificInstrumentField> unsubMarketlist;
list <CThostFtdcSpecificInstrumentField>::iterator unsubMarket_Iter;
list <CThostFtdcForQuoteRspField> forquotelist;
list <CThostFtdcForQuoteRspField>::iterator forquote_Iter;

#ifdef UNICODE 
typedef wstring tstring;
#define lsprintf_s swprintf_s 
#else
typedef string tstring;
#define lsprintf_s sprintf_s 
#endif

using std::cout;
using std::wcout;
using std::endl;
using std::cin;
using std::wcin;
#ifdef UNICODE
#define tcout wcout
#define tcin wcin
#else
#define tcout cout
#define tcin cin
#endif

strategys * its;
bool FindId2(const char *Instrument, const char * str)
{
	char * pdest1 = strstr((char*)Instrument, (char*)str);
	__int64  result1 = pdest1 - Instrument + 1;
	if (pdest1 != NULL)
		return true;
	else
		return false;
}

int GetType(const char * InstrumentID)
{
	int TYPEID = -1;
	if (FindId2(InstrumentID, "ni"))
	{
		TYPEID = TYPE_NI;
	}
	else if (FindId2(InstrumentID, "zn"))
	{
		TYPEID = TYPE_ZN;
	}
	else if (FindId2(InstrumentID, "al"))
	{
		TYPEID = TYPE_AL;
	}
	else if (FindId2(InstrumentID, "cu"))
	{
		TYPEID = TYPE_CU;
	}
	else if (FindId2(InstrumentID, "au"))
	{
		TYPEID = TYPE_AU;
	}
	else if (FindId2(InstrumentID, "ag"))
	{
		TYPEID = TYPE_AG;
	}
	else if (FindId2(InstrumentID, "i"))
	{
		TYPEID = TYPE_I;
	}
	else if (FindId2(InstrumentID, "ru"))
	{
		TYPEID = TYPE_RU;
	}
	else if (FindId2(InstrumentID, "ta"))
	{
		TYPEID = TYPE_TA;
	}
	else if (FindId2(InstrumentID, "a"))
	{
		TYPEID = TYPE_A;
	}
	else if (FindId2(InstrumentID, "m"))
	{
		TYPEID = TYPE_M;
	}
	else if (FindId2(InstrumentID, "y"))
	{
		TYPEID = TYPE_Y;
	}
	else if (FindId2(InstrumentID, "p"))
	{
		TYPEID = TYPE_P;
	}
	else if (FindId2(InstrumentID, "rb"))
	{
		TYPEID = TYPE_RB;
	}
	else if (FindId2(InstrumentID, "ma"))
	{
		TYPEID = TYPE_MA;
	}
	else if (FindId2(InstrumentID, "pp"))
	{
		TYPEID = TYPE_PP;
	}
	else if (FindId2(InstrumentID, "cs"))
	{
		TYPEID = TYPE_CS;
	}
	else if (FindId2(InstrumentID, "jd"))
	{
		TYPEID = TYPE_JD;
	}
	else if (FindId2(InstrumentID, "bu"))
	{
		TYPEID = TYPE_BU;
	}
	else if (FindId2(InstrumentID, "fg"))
	{
		TYPEID = TYPE_FG;
	}
	else if (FindId2(InstrumentID, "l"))
	{
		TYPEID = TYPE_L;
	}
	else if (FindId2(InstrumentID, "v"))
	{
		TYPEID = TYPE_V;
	}
	else if (FindId2(InstrumentID, "sr"))
	{
		TYPEID = TYPE_SR;
	}
	else if (FindId2(InstrumentID, "rm"))
	{
		TYPEID = TYPE_RM;
	}
	else if (FindId2(InstrumentID, "cf"))
	{
		TYPEID = TYPE_CF;
	}
	else if (FindId2(InstrumentID, "c"))
	{
		TYPEID = TYPE_C;
	}
	else if (FindId2(InstrumentID, "wh"))
	{
		TYPEID = TYPE_WH;
	}
	else if (FindId2(InstrumentID, "sm"))
	{
		TYPEID = TYPE_SM;
	}
	else if (FindId2(InstrumentID, "sf"))
	{
		TYPEID = TYPE_SF;
	}
	else if (FindId2(InstrumentID, "ic"))
	{
		TYPEID = TYPE_IC;
	}
	else if (FindId2(InstrumentID, "if"))
	{
		TYPEID = TYPE_IF;
	}
	else if (FindId2(InstrumentID, "ih"))
	{
		TYPEID = TYPE_IH;
	}
	else if (FindId2(InstrumentID, "t"))
	{
		TYPEID = TYPE_T;
	}
	else if (FindId2(InstrumentID, "tf"))
	{
		TYPEID = TYPE_TF;
	}
	return TYPEID;
}

typedef map<string, InstrumentInfo> QS_Strategy_Map;
typedef unordered_map<string, GuestOnlineHash> QS_Data_Map;

QS_Data_Map     mapData;
QS_Strategy_Map mapStrategy;
#include <iostream>
#include <sstream>
using namespace std;
std::string gMDFrontAddr[3];
std::string gBrokerID;
std::string gUserID;
std::string gPassword;
std::string sn;

char* ppInstrumentID[200] = {0};	

CThostFtdcDepthMarketDataField *depthdata[200] = {0};
int size = sizeof(CThostFtdcDepthMarketDataField);
int amount = 0;
std::map<std::string, int> gMarket;
 
int gStatus = 0;
CMdSpi mdspi;
 
struct Mdata
{
 int  processid;        //进程ID
 char InstrumentID[10]; //品种
};
#include <stdio.h>
#include <io.h>
int vnapi_Start()
{
	printf("vnapi classic(Market.CTP for Python)2.10\n");
	InitializeCriticalSection(&g_csdata);
	for (int i = 0; i < MAX_EVENTNUM; i++)
	{
		if (!hEvent[i])
	   {
		char temp[10] = { 0 };
		_snprintf_s(temp,sizeof(temp),sizeof(temp),"hEvent%d",i);
		hEvent[i] = CreateEvent(NULL, FALSE, FALSE, temp);
	   }
	}

	if (!access("./vnapictpmd.ini", 0))
	{
		char ip1[60] = { 0 };
		char ip2[60] = { 0 };
		char ip3[60] = { 0 };
		char investor[60] = { 0 };
		char brokerid[60] = { 0 };
		char password[60] = { 0 };
		GetPrivateProfileString("setting", "ip1", "", ip1, 60, "./vnapictpmd.ini");
		GetPrivateProfileString("setting", "ip1", "", ip2, 60, "./vnapictpmd.ini");
		GetPrivateProfileString("setting", "ip1", "", ip3, 60, "./vnapictpmd.ini");
		GetPrivateProfileString("setting", "investor", "", investor, 60, "./vnapictpmd.ini");
		GetPrivateProfileString("setting", "brokerid", "", brokerid, 60, "./vnapictpmd.ini");
		GetPrivateProfileString("setting", "password", "", password, 60, "./vnapictpmd.ini");
		gMDFrontAddr[0] = ip1;
		gMDFrontAddr[1] = ip2;
		gMDFrontAddr[2] = ip3;
		gUserID = investor;
		gBrokerID = brokerid;
		gPassword = password;
		std::cout << investor << "\n" << brokerid << "\n" << ip1 << "\n" << ip2 << "\n" << ip3 << std::endl;
		std::cout << "Login: " << gUserID << std::endl;
		BOOL ret = mdspi.InitMD();
		if (ret)
			return TRUE;
		else
			return FALSE;
	}
	else
	{
		std::cout << "fail to open vnapictpmd.ini"   << std::endl;
		gStatus = 1;
		return false;
	}
 
}

void UnSubscribeMarketData(const char *InstrumentID)
{
	char * crcvalue = NULL;

	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	memset(ppInstrumentID[amount],0,sizeof(TThostFtdcInstrumentIDType));
	_snprintf_s(ppInstrumentID[amount],
		sizeof(TThostFtdcInstrumentIDType),
		sizeof(TThostFtdcInstrumentIDType) - 1, "%s", InstrumentID);
	gMarket[InstrumentID] = amount;
	depthdata[amount] = new CThostFtdcDepthMarketDataField;
	memset(depthdata[amount],0,sizeof(CThostFtdcDepthMarketDataField));
	++amount;
	mdspi.UnSubscribeMarketData();
}

//询价
void SubscribeForQuoteRsp(  char *InstrumentID)
{
	/*
	char * crcvalue = NULL;
	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	::strcpy(ppInstrumentID[amount], InstrumentID);
	//::strcpy_s(ppInstrumentID[amount],sizeof(ppInstrumentID[amount]), contract);
	gMarket[InstrumentID] = amount;
	//data[amount] = new CThostFtdcDepthMarketDataField;
	depthdata[amount] = new CThostFtdcDepthMarketDataField;
	++amount;
	*/
	mdspi.SubscribeForQuoteRsp(InstrumentID);
}
//添加记录, 
bool FindId(const char *Instrument, const char * str)
{
	char * pdest1 = strstr((char*)Instrument, (char*)str);
	__int64  result1 = pdest1 - Instrument + 1;

	if (pdest1 != NULL)
		return true;
	else
		return false;
}
/*
bool Add(const char *InstrumentID, const PERIODTYPE * pt)
{
	std::unordered_map<string, GuestOnlineHash>::iterator it;
	it = mapData.find(InstrumentID);
	if (it == mapData.end())
	{
		GuestOnlineHash pi;
		//InitGuestOnlineHash(&pi);
		pair<string, GuestOnlineHash>value2(InstrumentID, pi);
		mapData.insert(value2);
	}
	it = mapData.find(InstrumentID);
	if (it != mapData.end())
	{
		GuestOnlineHash * q = &(it->second);
		return true;
	}
	return false;
}
*/

void SubscribeMarketData(char* InstrumentID)
{
	char * crcvalue = NULL;
	//Add( InstrumentID, NULL);
	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	_snprintf_s(ppInstrumentID[amount], sizeof(ppInstrumentID[amount]), sizeof(ppInstrumentID[amount]) - 1, "%s",
		 InstrumentID);
	gMarket[ InstrumentID] = amount;
	depthdata[amount] = new CThostFtdcDepthMarketDataField;
	++amount;
	mdspi.SubscribeMarketData();

}

bool vnapi_End()
{
	DeleteCriticalSection(&g_csdata);
	for (int i = 0; i < amount; ++i)
	{
		delete [](ppInstrumentID[i]);
		delete depthdata[i];
	}
	return true;
}

char *  GetApiVersion()
{
	printf("GetApiVersion\n");
	return mdspi.GetApiVersion();
}

TThostFtdcInstrumentIDTypeStruct InstrumentStruct;
char *  GetCmdContent_Tick()
{
	if (ticknamelist.size() > 0)
	{
		memset(&InstrumentStruct, 0, sizeof(TThostFtdcInstrumentIDTypeStruct));
		EnterCriticalSection(&g_csdata);
		memcpy_s(InstrumentStruct.Instrument, sizeof(TThostFtdcInstrumentIDTypeStruct), ticknamelist.begin()->Instrument, sizeof(TThostFtdcInstrumentIDType));
		ticknamelist.erase(ticknamelist.begin());
		LeaveCriticalSection(&g_csdata);
		return InstrumentStruct.Instrument;
	}
	else
		return NULL;
}

CThostFtdcRspInfoField Error;
void *  GetCmdContent_Error()
{
	if (errorlist.size() > 0)
	{
		memset(&Error, 0, sizeof(CThostFtdcRspInfoField));
		EnterCriticalSection(&g_csdata);
		Error.ErrorID = errorlist.begin()->ErrorID;
		memcpy_s(Error.ErrorMsg, sizeof(Error.ErrorMsg), errorlist.begin()->ErrorMsg, sizeof(Error.ErrorMsg));
		errorlist.erase(errorlist.begin());
		LeaveCriticalSection(&g_csdata);
		return  &Error;
	}
	else
		return NULL;
}

//订阅回调
CThostFtdcSpecificInstrumentField submarket;
void *  GetCmdContent_SubMarketData()
{
	if (subMarketlist.size() > 0)
	{
		memset(&submarket, 0, sizeof(CThostFtdcSpecificInstrumentField));
		EnterCriticalSection(&g_csdata);
		memcpy_s(submarket.InstrumentID, sizeof(submarket.InstrumentID), subMarketlist.begin()->InstrumentID, sizeof(submarket.InstrumentID));
		subMarketlist.erase(subMarketlist.begin());
		LeaveCriticalSection(&g_csdata);
		return  &submarket;
	}
	else
		return NULL;
}
//退订阅回调
CThostFtdcSpecificInstrumentField unsubmarket;
void *  GetCmdContent_UnSubMarketData()
{
	if (unsubMarketlist.size() > 0)
	{
		memset(&unsubmarket, 0, sizeof(CThostFtdcSpecificInstrumentField));
		EnterCriticalSection(&g_csdata);
		memcpy_s(unsubmarket.InstrumentID, sizeof(unsubmarket.InstrumentID), unsubMarketlist.begin()->InstrumentID, sizeof(unsubmarket.InstrumentID));
		unsubMarketlist.erase(unsubMarketlist.begin());
		LeaveCriticalSection(&g_csdata);
		return  &unsubmarket;
	}
	else
		return NULL;
}

//登录成功
CThostFtdcRspUserLoginField  logindata;
void *GetCmdContent_LoginScuess()
{
	if (loginlist.size() > 0)
	{
		memset(&logindata, 0, sizeof(CThostFtdcRspUserLoginField));
		EnterCriticalSection(&g_csdata);
		//TD移植来的，要检查字段是否吻合
		memcpy_s(logindata.TradingDay, sizeof(TThostFtdcDateType), loginlist.begin()->TradingDay, sizeof(TThostFtdcDateType));
		memcpy_s(logindata.LoginTime, sizeof(TThostFtdcTimeType), loginlist.begin()->LoginTime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindata.BrokerID, sizeof(TThostFtdcBrokerIDType), loginlist.begin()->BrokerID, sizeof(TThostFtdcBrokerIDType));
		memcpy_s(logindata.UserID, sizeof(TThostFtdcUserIDType), loginlist.begin()->UserID, sizeof(TThostFtdcUserIDType));
		memcpy_s(logindata.SystemName, sizeof(TThostFtdcSystemNameType), loginlist.begin()->SystemName, sizeof(TThostFtdcSystemNameType));
		logindata.FrontID = loginlist.begin()->FrontID;
		logindata.SessionID = loginlist.begin()->SessionID;
		memcpy_s(logindata.MaxOrderRef, sizeof(TThostFtdcOrderRefType), loginlist.begin()->MaxOrderRef, sizeof(TThostFtdcOrderRefType));
		memcpy_s(logindata.SHFETime, sizeof(TThostFtdcTimeType), loginlist.begin()->SHFETime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindata.DCETime, sizeof(TThostFtdcTimeType), loginlist.begin()->DCETime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindata.CZCETime, sizeof(TThostFtdcTimeType), loginlist.begin()->CZCETime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindata.FFEXTime, sizeof(TThostFtdcTimeType), loginlist.begin()->FFEXTime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindata.INETime, sizeof(TThostFtdcTimeType), loginlist.begin()->INETime, sizeof(TThostFtdcTimeType));
		loginlist.erase(loginlist.begin());
		LeaveCriticalSection(&g_csdata);
		return &logindata;
	}
	else
		return NULL;
}


//登录失败
CThostFtdcRspUserLoginField   logindatafailer;
void *GetCmdContent_LoginFailer()
{
	if (loginfailelist.size() > 0)
	{
		memset(&logindata, 0, sizeof(CThostFtdcRspUserLoginField));
		EnterCriticalSection(&g_csdata);
		memcpy_s(logindatafailer.TradingDay, sizeof(TThostFtdcDateType), loginfailelist.begin()->TradingDay, sizeof(TThostFtdcDateType));
		memcpy_s(logindatafailer.LoginTime, sizeof(TThostFtdcTimeType), loginfailelist.begin()->LoginTime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindatafailer.BrokerID, sizeof(TThostFtdcBrokerIDType), loginfailelist.begin()->BrokerID, sizeof(TThostFtdcBrokerIDType));
		memcpy_s(logindatafailer.UserID, sizeof(TThostFtdcUserIDType), loginfailelist.begin()->UserID, sizeof(TThostFtdcUserIDType));
		memcpy_s(logindatafailer.SystemName, sizeof(TThostFtdcSystemNameType), loginfailelist.begin()->SystemName, sizeof(TThostFtdcSystemNameType));
		logindatafailer.FrontID = loginfailelist.begin()->FrontID;
		logindatafailer.SessionID = loginfailelist.begin()->SessionID;
		memcpy_s(logindatafailer.MaxOrderRef, sizeof(TThostFtdcOrderRefType), loginfailelist.begin()->MaxOrderRef, sizeof(TThostFtdcOrderRefType));
		memcpy_s(logindatafailer.SHFETime, sizeof(TThostFtdcTimeType), loginfailelist.begin()->SHFETime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindatafailer.DCETime, sizeof(TThostFtdcTimeType), loginfailelist.begin()->DCETime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindatafailer.CZCETime, sizeof(TThostFtdcTimeType), loginfailelist.begin()->CZCETime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindatafailer.FFEXTime, sizeof(TThostFtdcTimeType), loginfailelist.begin()->FFEXTime, sizeof(TThostFtdcTimeType));
		memcpy_s(logindatafailer.INETime, sizeof(TThostFtdcTimeType), loginfailelist.begin()->INETime, sizeof(TThostFtdcTimeType));
		loginfailelist.erase(loginfailelist.begin());
		LeaveCriticalSection(&g_csdata);
		return &logindatafailer;
	}
	else
		return NULL;
}

CThostFtdcUserLogoutField   loginoutdata;
void *GetCmdContent_LoginOut()
{
	if (loginoutlist.size() > 0)
	{
		memset(&loginoutdata, 0, sizeof(CThostFtdcUserLogoutField));
		EnterCriticalSection(&g_csdata);
		memcpy_s(loginoutdata.BrokerID, sizeof(TThostFtdcBrokerIDType), loginoutlist.begin()->BrokerID, sizeof(TThostFtdcBrokerIDType));
		memcpy_s(loginoutdata.UserID, sizeof(TThostFtdcUserIDType), loginoutlist.begin()->UserID, sizeof(TThostFtdcUserIDType));
		loginoutlist.erase(loginoutlist.begin());
		LeaveCriticalSection(&g_csdata);
		return &loginoutdata;
	}
	else
		return NULL;
}

CThostFtdcForQuoteRspField   forquotedata; 
void *GetCmdContent_Forquote()
{
	if (forquotelist.size() > 0)
	{
		memset(&forquotedata, 0, sizeof(CThostFtdcForQuoteRspField));
		EnterCriticalSection(&g_csdata);
		//TD移植来的，要检查字段是否吻合
		memcpy_s(forquotedata.TradingDay, sizeof(TThostFtdcDateType), forquotelist.begin()->TradingDay, sizeof(TThostFtdcDateType));
		memcpy_s(forquotedata.InstrumentID, sizeof(TThostFtdcInstrumentIDType), forquotelist.begin()->InstrumentID, sizeof(TThostFtdcInstrumentIDType));
		memcpy_s(forquotedata.ForQuoteSysID, sizeof(TThostFtdcOrderSysIDType), forquotelist.begin()->ForQuoteSysID, sizeof(TThostFtdcOrderSysIDType));
		memcpy_s(forquotedata.ForQuoteTime, sizeof(TThostFtdcTimeType), forquotelist.begin()->ForQuoteTime, sizeof(TThostFtdcTimeType));
		memcpy_s(forquotedata.ActionDay, sizeof(TThostFtdcDateType), forquotelist.begin()->ActionDay, sizeof(TThostFtdcDateType));
		memcpy_s(forquotedata.ExchangeID, sizeof(TThostFtdcExchangeIDType), forquotelist.begin()->ExchangeID, sizeof(TThostFtdcExchangeIDType));
		forquotelist.erase(forquotelist.begin());
		LeaveCriticalSection(&g_csdata);
		return &forquotedata;
	}
	else
		return NULL;
}

int * connectdata = new int;
void *GetCmdContent_Connected()
{
	if (connectlist.size() > 0)
	{
		memset(connectdata, 0, sizeof(int));
		EnterCriticalSection(&g_csdata);
		//errdata->ErrorID = connectlist.begin()->ErrorID;
		//memcpy_s(errdata->ErrorMsg, sizeof(TThostFtdcErrorMsgType), connectlist.begin()->ErrorMsg, sizeof(TThostFtdcErrorMsgType));
		connectlist.erase(connectlist.begin());
		LeaveCriticalSection(&g_csdata);
		return connectdata;
	}
	else
		return NULL;
}

char *  GetTradingDay()
{
	return mdspi.GetTradingDay();
}
void   RegisterFront(char *pszFrontAddress)
{
	return mdspi.RegisterFront(pszFrontAddress);
}

void VNAPI_MD_API RegisterNameServer(char *pszNsAddress)
{
	return mdspi.RegisterNameServer(pszNsAddress);
}

int ReqUserLogin()
{
	return mdspi.ReqUserLogin();
}

int ReqUserLogout()
{
	return mdspi.ReqUserLogout();
}

bool  AddPeriodType2(const char *InstrumentID, int  periodtype)
{
	
	std::unordered_map<string, GuestOnlineHash>::iterator it;
	it = mapData.find(InstrumentID);
	if (it == mapData.end())
	{
		printf("*************没找到该合约的%s\n", InstrumentID);
		//GuestOnlineHash value;
		//InitGuestOnlineHash(&value);
		//mapData.insert(std::make_pair(InstrumentID, value));
		//ClearGuestOnlineHash(&value);
		//it = mapData.find(InstrumentID);
		return 0;
	}
	else
		GuestOnlineHash * q = &(it->second);
	return true;
}



void SaveTick(int index)
{
	if (index == 2)
	{
		CreateDirectoryA("TickData_Detail", NULL); //创建文件夹
		savetickstate = 2;
	}
	else if (index == 1)
	{
		CreateDirectoryA("TickData_Simple", NULL); //创建文件夹
		savetickstate = 1;
	}
}

void Log(const char * filename, const char * content)
{
	ifstream inf;
	ofstream ouf;
	inf.open(filename, ios::out);
	ofstream o_file(filename, ios::app);
	o_file << content <<  endl;
	o_file.close();	
}

void  SetRejectdataTime(double  begintime1, double endtime1, double begintime2, double endtime2, double begintime3, double endtime3, double begintime4, double endtime4)
{
	if (begintime1 < 0 || endtime1 < 0 || begintime2 < 0 || endtime2 < 0 || begintime3 < 0 || endtime3 < 0 || begintime4 < 0 || endtime4 < 0)
	{
		printf("设置拒收行情的时间段必须大于等于0\n");
	}
	if (begintime1 != 100 && endtime1 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime1, endtime1);
		mdspi.begintime1 = begintime1;
		mdspi.endtime1 = endtime1;
	}
	if (begintime2 != 100 && endtime2 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime2, endtime2);
	    mdspi.begintime2 = begintime2;
	    mdspi.endtime2 = endtime2;
	}
	if (begintime3 != 100 && endtime3 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime3, endtime3);
	  mdspi.begintime3 = begintime3;
	  mdspi.endtime3 = endtime3;

	}
	if (begintime3 != 100 && endtime3 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime4, endtime4);
	  mdspi.begintime4 = begintime4;
	  mdspi.endtime4 = endtime4;
	}
}

int OnCmd()
{
	DWORD dw = WaitForMultipleObjects(MAX_EVENTNUM, hEvent, FALSE, INFINITE);
	switch (dw)
	{
	case WAIT_OBJECT_0 + EID_OnFrontDisconnected:
		// The process identified by h[0] (hProcess1) terminated.
		return MD_NETCONNECT_BREAK;
		break;
	case WAIT_OBJECT_0 + EID_OnFrontConnected:
		// The process identified by h[1] (hProcess2) terminated.
		return MD_NETCONNECT_SCUESS;
		break;
	case WAIT_OBJECT_0 + EID_OnRspUserLogin_Failer:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_LOGIN_DENIED;
		break;
	case WAIT_OBJECT_0 + EID_OnRspUserLogin_Scuess:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_LOGIN_SCUESS;
		break;
	case WAIT_OBJECT_0 + EID_OnRtnDepthMarketData:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_NEWTICK;
		break;
	case WAIT_OBJECT_0 + EID_IsErrorRspInfo:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_ERROR;
		break;
	case WAIT_OBJECT_0 + EID_OnRspSubMarketData:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_SUBCRIBE_SCUESS;
		break;
	case WAIT_OBJECT_0 + EID_OnRspUnSubMarketData:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_UNSUBCRIBE_SCUESS;
		break;
	case WAIT_OBJECT_0 + EID_OnRspUserLogout:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_LOGINOUT_SCUESS;
		break;
	case WAIT_OBJECT_0 + EID_OnRspForQuote:
		// The process identified by h[2] (hProcess3) terminated.
		return MD_QRY_FORQUOTE;
		break;
	}
	return MD_EMPTY;
}

int GetUnGetCmdSize()
{
	return  (int)cmdlist.size();
}

int  GetCmd()
{
	if (cmdlist.size() <= 0)
		return  MD_EMPTY;
	EnterCriticalSection(&g_csdata);
	int cmdtype = cmdlist.begin()->cmd;
	return cmdtype;
}

char listcmd[31] = { 0 };
char *   GetCmdContent()
{
	if (cmdlist.size() <= 0)
		return "";
	memset(&listcmd, 0, sizeof(listcmd));
	memcpy_s(&listcmd, sizeof(listcmd), cmdlist.begin()->content, sizeof(listcmd));
	cmdlist.erase(cmdlist.begin());
	LeaveCriticalSection(&g_csdata);
	return listcmd;
}

void *GetData(int i)
{
	if (gStatus)return NULL;
	if (i < 0 || (i >= (int)gMarket.size()))
		return NULL;
	else
		return depthdata[i];
}

int GetInstrumentNum()
{
	return (int)gMarket.size();
}

int IsInitOK()
{
	return mdspi.IsInitOK();
}
