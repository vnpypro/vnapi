/*
（1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
适合学习入门和扩展开发，就在1.0版发布后不久，非法金融经营者就对我的vnapi 1.0（当时不叫vnapi）进行了不实的舆论攻击，
从那次被诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？

当初我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
写出了virtualapi仿真回测（盛立版、CTP版、通联沪深A股Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆，2019年我又回到了上海，
在最近刀郎事件发生后，我开始怀疑我当初离开上海的想法和刀郎的想法是一样的。

由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
其带有非法金融交易功能的软件甚至短暂的上了某期货公司官网，后他们又于2021年11月被上海证监局调查。
在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，说不清楚这样做是为了自救，
还是别的原因，但我忠心的希望金融从业者们牢记合规底线，勿给非法金融可乘之机。

为记住那段历史，本次发布的vnapi2.10 期货CTP接口底层代码，正是源于2017年4月的1.0开源版本的稍作整理，已升级到最新的CTP版本，
1.0版就是被诋毁造谣的那个版本，vnapi改名前的1.0 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
现在vnapi2.0作为一款Python CTP接口的框架，和当初1.0一样，是基于Ctypes技术，简单易学，主要目的是为了为开发者
提供一个通俗易懂的python后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。

我还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn）， 基于我开发的vnapi的另一个回调版本，
面向的是专业机构客户，VNTrader系列可以说在vnapi 1.0后端框架技术上迭代产生的，
当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于我和上海量贝软件的原创产品，
我于2014年还获得基于AR增强现实技术的国家发明专利一项。
我另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。

承载着7年的维权经历与梦想，引来了vnapi2.10版本的发布，这个产品定位是作抛砖引玉之用，适合用户学习Python量化和扩展功能，
而非堆砌功能，请各位以宽容的眼光去对待他。

（2）vnapi 遵循 开源协议GPL v3
简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
官方网站：http://www.vnpy.cn
*/
#pragma once
#include <iostream>
using  namespace std;
#include <string.h>
#define NULL 0
#include <math.h>
#define  MAX_TICK_NUM   60//120//60  //基本数据，之取第一个下标位0的元素，数量无所谓
#define  MAX_TICK_MOVE_NUM   59
#define  MAX_USERNAME     33


#define  TYPE_NI   100000
#define  TYPE_ZN   100001
#define  TYPE_AL   100002
#define  TYPE_CU   100003
#define  TYPE_AU   100004
#define  TYPE_AG   100005
#define  TYPE_I    100006
#define  TYPE_RU   100007
#define  TYPE_TA   100008
#define  TYPE_A    100009
#define  TYPE_M    100010
#define  TYPE_Y    100011
#define  TYPE_P    100012
#define  TYPE_RB   100013
#define  TYPE_MA   100014
#define  TYPE_PP   100015
#define  TYPE_CS   100016
#define  TYPE_JD   100017
#define  TYPE_BU   100018
#define  TYPE_FG   100019
#define  TYPE_L    100020
#define  TYPE_V    100021
#define  TYPE_J    100022
#define  TYPE_SR   100023
#define  TYPE_RM   100024
#define  TYPE_CF   100025
#define  TYPE_C    100026
#define  TYPE_WH   100027
#define  TYPE_SM   100028
#define  TYPE_SF   100029
#define  TYPE_PB   100030
#define  TYPE_SN   100031
#define  TYPE_WR   100032
#define  TYPE_HC   100033
#define  TYPE_FU   100034
#define  TYPE_IC   100035
#define  TYPE_IF   100036
#define  TYPE_IH   100037
#define  TYPE_T    100038
#define  TYPE_TF   100039

struct GuestOnlineHash
{
	int key;
	char InstrumentID[10];
	int  TYPEID;
	char	TickFileWritepaths[20];
	int  tick_VolumeLast;
	bool	ReceiveTick;
	bool	FristTick;
	bool	LastTick;
	int avespace;
	int tradenum;
	char keystr[MAX_USERNAME];
	double nottime[5];
	double starttime[5];
	double endtime[5];
	double	tick_data[10];				//TICK基本数据
	double	tick_AskPrice1[MAX_TICK_NUM];			//买一价
	double	tick_BidPrice1[MAX_TICK_NUM];			//卖一价
	double	tick_AskVolume1[MAX_TICK_NUM];		//买一量
	double	tick_BidVolume1[MAX_TICK_NUM];		//卖一量
	double	tick_Volume[MAX_TICK_NUM];			//成交量
};


struct strategys
{
	char instructment[20];
	int type;
	int strategy[100];
};

struct Instructment
{
	char instructment[20];
	int type;
	int MD_begin1;
	int MD_end1;
	int MD_begin2;
	int MD_end2;
	int MD_begin3;
	int MD_end3;
	int MD_begin4;
	int MD_end4;
	int MD_begin5;
	int MD_end5;
	int TD_begin1;
	int TD_end1;
	int TD_begin2;
	int TD_end2;
	int TD_begin3;
	int TD_end3;
	int TD_begin4;
	int TD_end4;
	int TD_begin5;
	int TD_end5;
};


struct hashinfo
{
	double usedrate;
	double max_len;
	double ave_len;
	int maxlen_num;
};



struct TThostFtdcInstrumentIDTypeStruct
{
	TThostFtdcInstrumentIDType Instrument;
};

struct InstrumentInfo
{
	string strategyfile[300];
	//int strategy[100];
	int strategyfilenum;
	int position[100];
	bool tradestate;
};

#define  EID_OnFrontDisconnected   0
#define  EID_OnFrontConnected      1
#define  EID_OnRspUserLogin_Failer 2
#define  EID_OnRspUserLogin_Scuess 3
#define  EID_OnRtnDepthMarketData  4
#define  EID_IsErrorRspInfo        5
#define  EID_OnRspSubMarketData    6
#define  EID_OnRspUnSubMarketData  7
#define  EID_OnRspUserLogout       8
#define  EID_OnRspForQuote         9

//回调类型
#define MD_EMPTY              8000 //无
#define MD_LOGIN_SCUESS       8001 //登录成功
#define MD_LOGIN_DENIED       8002 //登录被拒绝
#define MD_LOGINOUT_SCUESS    8004 //登出成功
#define MD_NETCONNECT_SCUESS  8005 //连接成功
#define MD_NETCONNECT_BREAK   8006 //断开连接
#define MD_NETCONNECT_FAILER  8007 //连接失败
#define MD_SUBCRIBE_SCUESS    8008 //订阅成功
#define MD_UNSUBCRIBE_SCUESS  8009 //取消订阅成功
#define MD_NEWTICK            8010 //新Tick到来
#define MD_ERROR              8011 //错误应答
#define MD_QRY_FORQUOTE       8012 //询价通知

