/*
（1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
适合学习入门和拓展开发，就在1.0版发布后不久，我的vnapi 1.0（当时不叫vnapi）就遭受了非法金融经营者用1.2万字连载诽谤文的诋毁攻击，
他们甚至在诽谤文下方还放上他们自己的涉及非法金融开源软件的广告。
从那次诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？
当初遇此情形，我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
写出了virtualapi仿真回测（盛立版、CTP版、通联Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆。
由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
其产品甚至短暂的上了某期货公司官网。
在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，我忠心的希望金融从业者们牢记合规底线，
勿给非法金融可乘之机。
本次发布的vnapi2.10 期货CTP接口行情库底层代码，源于2017年4月的1.0开源版本，已更新至支持新版的CTP接口。
改名前的2.09 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
这6年时间，我们一边开发产品，一边开始了长达6年的维权之路。
vnapi2.0作为一款Python CTP接口的框架，基于Ctypes技术，简单易学，主要目的是为了为开发者提供一个通俗易懂
的后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。
本团队还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn），
面向的是专业机构客户，VNTrader系列均是在vnapi 1.0后端框架技术上迭代产生的，
当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于quant林和上海量贝软件的原创产品，
quant林于2014年获得基于AR增强现实技术的国家发明专利一项。
quant林另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。

（2）vnapi 遵循 开源协议GPL v3
简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
官方网站：http://www.vnpy.cn
*/
#include "stdafx.h"
#include "../common/common.h"
#include "MdSpi.h"
#include <map>
#include <string>
#include <algorithm>
#include "iostream"
#include <windows.h>

#include "def.h"
#include <process.h>
#include <ShellApi.h>
#include <fstream> 
using namespace std;
#pragma warning(disable : 4996)
#include <process.h>
extern HANDLE hEvent[MAX_EVENTNUM];
extern std::string gMDFrontAddr[3];
extern std::string gBrokerID;
extern std::string gUserID;
extern std::string gPassword;

extern char* ppInstrumentID[];	
extern CThostFtdcDepthMarketDataField *depthdata[];
extern int size;
extern int amount;
extern std::map<std::string, int> gMarket;
extern CThostFtdcDepthMarketDataField* depthdata1;
extern int size;
extern int amount;

CRITICAL_SECTION g_csdata;
extern list <CMDCONTENT> cmdlist;
extern list <TThostFtdcInstrumentIDTypeStruct> ticknamelist;
extern list <CThostFtdcRspInfoField> errorlist;
extern list <CThostFtdcRspUserLoginField> loginlist;
extern list <CThostFtdcRspUserLoginField> loginfailelist;
extern list <CThostFtdcUserLogoutField> loginoutlist;
extern list <int> connectlist;
extern list <CThostFtdcSpecificInstrumentField> subMarketlist;
extern list <CThostFtdcSpecificInstrumentField> unsubMarketlist;
extern list <CThostFtdcForQuoteRspField> forquotelist;

typedef map<string, InstrumentInfo> QS_Strategy_Map;
typedef unordered_map<string, GuestOnlineHash> QS_Data_Map;
extern QS_Data_Map     mapData;
extern QS_Strategy_Map mapStrategy;

int  GetMin(double thistime)
{
	int h = (int)(thistime * 100); //  21
	int m = (int)(thistime * 10000) - 100 * h; //  2130-2100=30
	return  (h * 60 + m);
}
string dbtoch(double nums)		
//将double 转换为 string 进行保存
{
	char chr[20] = { 0 };
	sprintf(chr, "%.0f", nums);
	string tt = chr;
	return tt;
}

inline bool  CheckData(double data)
{
	if ((!_isnan(data)) && data > 1e-10)
		return true;
	return false;
}
inline bool  CheckDataOnlyNum(double data)
{
	if (!_isnan(data))
		return true;
	return false;
}
double zero_max(double a, double b)
{
	if (a == 0 && b == 0)
		return 0;
	else if (a == 0)
		return b;
	else if (b == 0)
		return a;
	else
		return max(a, b);
}

double zero_min(double a, double b)
{
	if (a == 0 && b == 0)
		return 0;
	else if (a == 0)
		return b;
	else if (b == 0)
		return a;
	else
		return min(a, b);
}

BOOL CMdSpi::InitMD()
{
	char dir[256] = { 0 };
	::GetCurrentDirectory(255, dir);
	std::string tempDir = std::string(dir).append("\\MdTemp\\");
	::CreateDirectory(tempDir.c_str(), NULL);
	mdapi = CThostFtdcMdApi::CreateFtdcMdApi(tempDir.c_str());
	mdapi->RegisterSpi(this);
	mdapi->RegisterFront((char *)gMDFrontAddr[0].c_str());
	mdapi->RegisterFront((char *)gMDFrontAddr[1].c_str());
	mdapi->RegisterFront((char *)gMDFrontAddr[2].c_str());
	std::cout << "InitMd..." << std::endl;
	mdapi->Init();
	return TRUE;
}

CMdSpi::CMdSpi()
{
	memset(tradingday,0,sizeof(tradingday));
	memset(ver, 0, sizeof(ver));
	mInitOK = FALSE;
	mdapi = NULL;
	mhSyncObj = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	RequestID = 0;
	begintime1=-1;
	begintime2=-1;
	begintime3=-1;
	begintime4=-1;
	endtime1=-1;
	endtime2=-1;
	endtime3=-1;
	endtime4=-1;
}

CMdSpi::~CMdSpi()
{
	::CloseHandle(mhSyncObj);
}

void CMdSpi::OnRspError(CThostFtdcRspInfoField *pRspInfo,int nRequestID, bool bIsLast)
{
	IsErrorRspInfo(pRspInfo);
}

void CMdSpi::OnFrontDisconnected(int nReason)
{
	SYSTEMTIME t;
	::GetLocalTime(&t);
	std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
 	//::Beep(800, 10000);
	//CMDCONTENT tn;
	//memset(&tn, 0, sizeof(CMDCONTENT));
	//strncpy_s(tn.content, sizeof(tn.content), "与行情服务器断开连接", sizeof("与行情服务器断开连接"));
	//tn.cmd = MD_NETCONNECT_BREAK;
	//EnterCriticalSection(&g_csdata);
	//cmdlist.push_back(tn);
	//LeaveCriticalSection(&g_csdata);
	//LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnFrontDisconnected]);
}

void CMdSpi::OnHeartBeatWarning(int nTimeLapse)
{
}

void CMdSpi::OnFrontConnected()
{
	ReqUserLogin();
	CMDCONTENT tn;
	memset(&tn, 0, sizeof(CMDCONTENT));
	tn.cmd = MD_NETCONNECT_SCUESS;
	EnterCriticalSection(&g_csdata);
	cmdlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnFrontConnected]);
	//CMDCONTENT tn;
	//memset(&tn, 0, sizeof(CMDCONTENT));
	//strncpy_s(tn.content, sizeof(tn.content), "连接行情服务器成功", sizeof("连接行情服务器成功"));
	//tn.cmd = MD_NETCONNECT_SCUESS;
	//EnterCriticalSection(&g_csdata);
	//cmdlist.push_back(tn);
	//LeaveCriticalSection(&g_csdata);
	//SetEvent(hEvent[EID_OnFrontConnected]);
	///用户登录请求
	//ReqUserLogin();
}


char *CMdSpi::GetApiVersion()
{
	_snprintf_s(ver,sizeof(ver),sizeof(ver),"%s", (char*)(mdapi->GetApiVersion()));
	return ver;
}

char *CMdSpi::GetTradingDay()
{
	_snprintf_s(tradingday, sizeof(tradingday), sizeof(tradingday), "%s", (char*)(mdapi->GetTradingDay()));
	return tradingday;
}

void   CMdSpi::RegisterFront(char *pszFrontAddress)
{
	mdapi->RegisterFront(pszFrontAddress);
}

void   CMdSpi::RegisterNameServer(char *pszNsAddress)
{
	mdapi->RegisterNameServer(pszNsAddress);
}

int CMdSpi::ReqUserLogin()
{
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	_snprintf_s(req.BrokerID, sizeof(req.BrokerID), sizeof(req.BrokerID) - 1, "%s", gBrokerID.c_str());
	_snprintf_s(req.UserID, sizeof(req.UserID), sizeof(req.UserID) - 1, "%s", gUserID.c_str());
	_snprintf_s(req.Password, sizeof(req.Password), sizeof(req.Password) - 1, "%s", gPassword.c_str());
	int iResult = mdapi->ReqUserLogin(&req, ++RequestID);
	if (iResult != 0)
		cerr << "Failer: ReqUserLogin" << endl;
	return iResult;
}

int CMdSpi::ReqUserLogout()
{
	std::cout << "--->>> " << __FUNCTION__ << std::endl;
	CThostFtdcUserLogoutField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, gBrokerID.c_str());
	strcpy(req.UserID, gUserID.c_str());
	//strcpy(req.Password, gPassword.c_str());
	int iResult = mdapi->ReqUserLogout(&req, ++RequestID);
	if (iResult != 0)
		cout << "Failer: ReqUserLogout" << endl;
	return iResult;
}

void CMdSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (!pRspInfo)return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		std::cout << "TradingDay: " << mdapi->GetTradingDay() << std::endl;
		mInitOK = TRUE;
		if (pRspInfo && pRspInfo->ErrorID != 0)
		{
			std::cout << "Failer: " << __FUNCTION__ << pRspInfo->ErrorID << pRspInfo->ErrorMsg << std::endl;
			//printf("OnRspUserLogin ,ErrorID=0x%04x, ErrMsg=%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
			CThostFtdcRspUserLoginField tn;
			memset(&tn, 0, sizeof(CThostFtdcRspUserLoginField));
			memcpy_s(&tn, sizeof(CThostFtdcRspUserLoginField), pRspUserLogin, sizeof(CThostFtdcRspUserLoginField));
			//strncpy_s(tn.content, sizeof(tn.content), "登录行情服务器失败", sizeof("登录行情服务器失败"));
			//tn.cmd = MD_LOGIN_DENIED;
			EnterCriticalSection(&g_csdata);
			loginfailelist.push_back(tn);
			LeaveCriticalSection(&g_csdata);
			SetEvent(hEvent[EID_OnRspUserLogin_Failer]);
		}
		else
		{
			std::cout << "Scuess: "<< __FUNCTION__ << std::endl;
			CThostFtdcRspUserLoginField tn;
			memset(&tn, 0, sizeof(CThostFtdcRspUserLoginField));
			//strncpy_s(tn.content, sizeof(tn.content), "登录行情服务器成功", sizeof("登录行情服务器成功"));
			memcpy_s(&tn, sizeof(CThostFtdcRspUserLoginField), pRspUserLogin, sizeof(CThostFtdcRspUserLoginField));
			//tn.cmd = MD_LOGIN_SCUESS;
			EnterCriticalSection(&g_csdata);
			loginlist.push_back(tn);
			LeaveCriticalSection(&g_csdata);
			//LeaveCriticalSection(&g_csdata);
			SetEvent(hEvent[EID_OnRspUserLogin_Scuess]);
			// ReadInstrument(1);
			//SubscribeMarketData("600119",1);
			Sleep(3000);
		}
	}
}

void CMdSpi::SubscribeForQuoteRsp(  char * InstrumentID)
{
	int iResult = mdapi->SubscribeForQuoteRsp(&InstrumentID,1);
	if (iResult != 0)
		cerr << "Failer:SubscribeForQuoteRsp[" << InstrumentID << "]"   << endl;
}

void CMdSpi::SubscribeMarketData()
{
	int iResult = mdapi->SubscribeMarketData( &(ppInstrumentID[amount - 1]), 1);
	if (iResult != 0)
		cout << "Failer:SubscribeMarketData["<< ppInstrumentID[amount - 1] << "]" <<endl;
}

void CMdSpi::UnSubscribeMarketData()
{
	int iResult = mdapi->UnSubscribeMarketData(&(ppInstrumentID[amount - 1]), 1);
	if (iResult != 0)
		cout << "Failer:UnSubscribeMarketData[" << ppInstrumentID[amount - 1] << "]" <<   endl;
}

void CMdSpi::WirteTick_Simple(const char * InstrumentID,const char * ticktime, double price)
{
	char str[200] = { 0 };
	strcat_s(str, 200, "TickData_Simple\\TickData_");
	strcat_s(str, 200, InstrumentID);
	strcat_s(str, 200, ".csv");
	ifstream inf;
	ofstream ouf;
	inf.open(str, ios::out);
	ofstream o_file(str, ios::app);
	o_file << ticktime<<","<<price << "," << endl;
	o_file.close();
}


void CMdSpi::WirteTick_Detail(const char * InstrumentID,const CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	char str[200] = { 0 };
	strcat_s(str, 200, "TickData_Detail\\TickData_");
	strcat_s(str, 200, InstrumentID);
	strcat_s(str, 200, ".csv");
	ifstream inf;
	ofstream ouf;
	inf.open(str, ios::out);
	//}
	if (!inf)
	{
		cout << "文件不能打开" << endl;
		ofstream o_file(str, ios::app);
		o_file << "交易日" << ","
			<< "合约" << ","
			<< "时间" << ","
			<< "毫秒" << ","
			<< "现价" << ","
			<< "持仓" << ","  //持仓量
			<< "成交量" << ","
			<< "买一" << ","
			//<< "买二"<< ","
			//<< "买三" << ","
			//<< "买四" << ","
			//<< "买五" << ","
			<< "买一量" << ","
			//<< "买二量" << ","
			//<< "买三量" << ","
			//<< "买四量" << ","
			//<< "买五量" << ","
			<< "卖一" << ","
			//<< "卖二" << ","
			//<< "卖三" << ","
			//<< "卖四" << ","
			//<< "卖五" << ","
			<< "卖一量" << ","
		    //<< "卖二量" << ","
			//<< "卖三量" << ","
			//<< "卖四量" << ","
			//<< "卖五量" << ","
			<< "涨停价" << ","
			<< "跌停价" << endl;
	}
	else
	{
		ofstream o_file(str, ios::app);
	}
	ofstream o_file(str, ios::app);
	o_file<< pDepthMarketData->TradingDay << ","
		<< pDepthMarketData->InstrumentID << ","
		<< pDepthMarketData->UpdateTime << ","
		<< pDepthMarketData->UpdateMillisec << ","
		<< pDepthMarketData->LastPrice<< ","
		<< pDepthMarketData->OpenInterest << ","  //持仓量
		<< pDepthMarketData->Volume << ","
		<< pDepthMarketData->AskPrice1 << ","
		//<< pDepthMarketData->AskPrice2 << ","
		//<< pDepthMarketData->AskPrice3 << ","
		//<< pDepthMarketData->AskPrice4 << ","
		//<< pDepthMarketData->AskPrice5 << ","
		<< pDepthMarketData->AskVolume1 << ","
		//<< pDepthMarketData->AskVolume2 << ","
		//<< pDepthMarketData->AskVolume3 << ","
		//<< pDepthMarketData->AskVolume4 << ","
		//<< pDepthMarketData->AskVolume5 << ","
		<< pDepthMarketData->BidPrice1 << ","
		//<< pDepthMarketData->BidPrice2 << ","
		//<< pDepthMarketData->BidPrice3 << ","
		//<< pDepthMarketData->BidPrice4 << ","
		//<< pDepthMarketData->BidPrice5 << ","
		<< pDepthMarketData->BidVolume1 << ","
		//<< pDepthMarketData->BidVolume2 << ","
		//<< pDepthMarketData->BidVolume3 << ","
		//<< pDepthMarketData->BidVolume4 << ","
		//<< pDepthMarketData->BidVolume5 << ","
		<< pDepthMarketData->UpperLimitPrice << ","
		<< pDepthMarketData->LowerLimitPrice << endl;
	o_file.close();
}


bool firststate = true;

HANDLE hThread[12];
unsigned int uiThreadId[12];
int savetickstate = 0;

double GetLocalTimeSec2()
{
	SYSTEMTIME sys_time;
	GetLocalTime(&sys_time);
	double system_times;
	system_times = (double)((sys_time.wHour) / 10e1) + (double)((sys_time.wMinute) / 10e3) + (double)((sys_time.wSecond) / 10e5);	//格式时间0.145100
	return system_times;
}

void CMdSpi::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{	
	if (!pDepthMarketData)
		return;
	double Nowtime = GetLocalTimeSec2();
	if (begintime1 > -0.1 && endtime1 > -0.1)
	{
		if (Nowtime<begintime1 && Nowtime>endtime1)
	  {
		printf("不在交易时间内，行情数据被拒绝[%s %0.02f %s]\n", pDepthMarketData->InstrumentID, pDepthMarketData->LastPrice, pDepthMarketData->UpdateTime);
		return;
	  }
	}
	if (begintime2 > -0.1 && endtime2 > -0.1)
	{
		if (Nowtime > begintime2 && Nowtime < endtime2)
		{
			printf("不在交易时间内，行情数据被拒绝[%s %0.02f %s]\n", pDepthMarketData->InstrumentID, pDepthMarketData->LastPrice, pDepthMarketData->UpdateTime);
			//WirteTradeRecordToFileMainThreadB(Nowtime, "不在交易时间内，直接返回");
			return;
		}
	}
	if (begintime3 > -0.1 && endtime3 > -0.1)
	{
		if (Nowtime > begintime3 && Nowtime < endtime3)
		{
			printf("不在交易时间内，行情数据被拒绝[%s %0.02f %s]\n", pDepthMarketData->InstrumentID, pDepthMarketData->LastPrice, pDepthMarketData->UpdateTime);
			//WirteTradeRecordToFileMainThreadB(Nowtime, "不在交易时间内，直接返回");
			return;
		}
	}
	if (begintime4 > -0.1 && endtime4 > -0.1)
	{
		if (Nowtime > begintime4 && Nowtime < endtime4)
		{
			printf("不在交易时间内，行情数据被拒绝[%s %0.02f %s]\n", pDepthMarketData->InstrumentID, pDepthMarketData->LastPrice, pDepthMarketData->UpdateTime);
			//WirteTradeRecordToFileMainThreadB(Nowtime, "不在交易时间内，直接返回");
			return;
		}
	}
	if (gMarket.find(pDepthMarketData->InstrumentID) != gMarket.end())
	{
		memcpy(depthdata[gMarket[pDepthMarketData->InstrumentID]], pDepthMarketData, sizeof(CThostFtdcDepthMarketDataField));
		printf("savetickstate:[%d]\n", savetickstate);
		if (savetickstate==2)
		{   
			printf("保存数据(详细)中...\r");
			WirteTick_Detail((char*)pDepthMarketData->InstrumentID, pDepthMarketData);
		}
		else if (savetickstate == 1)
		{
			printf("保存数据(少量)中...\r");
			WirteTick_Simple((char*)pDepthMarketData->InstrumentID, (char*)pDepthMarketData->UpdateTime, (double)pDepthMarketData->LastPrice);
		}
		TThostFtdcInstrumentIDTypeStruct tn;
		memset(&tn, 0, sizeof(TThostFtdcInstrumentIDTypeStruct));
		strncpy_s(tn.Instrument, sizeof(tn.Instrument), pDepthMarketData->InstrumentID, sizeof(pDepthMarketData->InstrumentID));
		//tn.cmd = MD_NEWTICK;
		EnterCriticalSection(&g_csdata);
		ticknamelist.push_back(tn);
		//ticknamelist.push_back(*(TThostFtdcInstrumentIDType*)(&tt));
		LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_OnRtnDepthMarketData]);
	}
}

bool CMdSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		SYSTEMTIME t;
		::GetLocalTime(&t);
		std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
		std::cout << "--->>> ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << std::endl;
		CThostFtdcRspInfoField tn;
		memset(&tn, 0, sizeof(CThostFtdcRspInfoField));
		tn.ErrorID = pRspInfo->ErrorID;
		strncpy_s(tn.ErrorMsg, sizeof(tn.ErrorMsg), pRspInfo->ErrorMsg, sizeof(pRspInfo->ErrorMsg));
		EnterCriticalSection(&g_csdata);
		errorlist.push_back(tn);
		LeaveCriticalSection(&g_csdata);
		SetEvent(hEvent[EID_IsErrorRspInfo]);
	}
	return bResult;
}

///订阅行情应答
void  CMdSpi::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	CThostFtdcSpecificInstrumentField tn;
	memset(&tn, 0, sizeof(CThostFtdcSpecificInstrumentField));
	strncpy_s(tn.InstrumentID, sizeof(tn.InstrumentID), "订阅成功", sizeof("订阅成功"));
	//tn.cmd = MD_SUBCRIBE_SCUESS;
	EnterCriticalSection(&g_csdata);
	subMarketlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	//LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRspSubMarketData]);
};

///取消订阅行情应答
void  CMdSpi::OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	CThostFtdcSpecificInstrumentField tn;
	memset(&tn, 0, sizeof(CThostFtdcSpecificInstrumentField));
	strncpy_s(tn.InstrumentID, sizeof(tn.InstrumentID), "取消订阅成功", sizeof("取消订阅成功"));
	//tn.cmd = MD_UNSUBCRIBE_SCUESS;
	EnterCriticalSection(&g_csdata);
	unsubMarketlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	//LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRspUnSubMarketData]);
};

///询价通知
void  CMdSpi::OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp)
{
	CThostFtdcForQuoteRspField tn;
	memset(&tn, 0, sizeof(CThostFtdcForQuoteRspField));
	memcpy_s(&tn,sizeof(CThostFtdcForQuoteRspField), pForQuoteRsp, sizeof(CThostFtdcForQuoteRspField));
	//strncpy_s(tn.InstrumentID, sizeof(tn.InstrumentID), "取消订阅成功", sizeof("取消订阅成功"));
	//tn.cmd = MD_UNSUBCRIBE_SCUESS;
	EnterCriticalSection(&g_csdata);
	forquotelist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	//LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRspForQuote]);
};

///登出请求响应
void CMdSpi::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	CThostFtdcUserLogoutField tn;
	memset(&tn, 0, sizeof(CThostFtdcUserLogoutField));
	strncpy_s(tn.BrokerID, sizeof(tn.BrokerID), pUserLogout->BrokerID, sizeof(tn.BrokerID));
	strncpy_s(tn.UserID, sizeof(tn.UserID), pUserLogout->UserID, sizeof(tn.UserID));
	//tn.cmd = MD_LOGINOUT_SCUESS;
	EnterCriticalSection(&g_csdata);
	loginoutlist.push_back(tn);
	LeaveCriticalSection(&g_csdata);
	//LeaveCriticalSection(&g_csdata);
	SetEvent(hEvent[EID_OnRspUserLogout]);
}