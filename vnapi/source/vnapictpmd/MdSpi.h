/*
（1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
适合学习入门和拓展开发，就在1.0版发布后不久，我的vnapi 1.0（当时不叫vnapi）就遭受了非法金融经营者用1.2万字连载诽谤文的诋毁攻击，
他们甚至在诽谤文下方还放上他们自己的涉及非法金融开源软件的广告。
从那次诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？
当初遇此情形，我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
写出了virtualapi仿真回测（盛立版、CTP版、通联Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆。
由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
其产品甚至短暂的上了某期货公司官网。
在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，我忠心的希望金融从业者们牢记合规底线，
勿给非法金融可乘之机。
本次发布的vnapi2.10 期货CTP接口行情库底层代码，源于2017年4月的1.0开源版本，已更新至支持新版的CTP接口。
改名前的2.09 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
这6年时间，我们一边开发产品，一边开始了长达6年的维权之路。
vnapi2.0作为一款Python CTP接口的框架，基于Ctypes技术，简单易学，主要目的是为了为开发者提供一个通俗易懂
的后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。
本团队还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn），
面向的是专业机构客户，VNTrader系列均是在vnapi 1.0后端框架技术上迭代产生的，
当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于quant林和上海量贝软件的原创产品，
quant林于2014年获得基于AR增强现实技术的国家发明专利一项。
quant林另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。

（2）vnapi 遵循 开源协议GPL v3
简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
官方网站：http://www.vnpy.cn
*/
#ifndef MDSPI_H
#define MDSPI_H

class CMdSpi : public CThostFtdcMdSpi
{
public:
	char ver[30] ;
	char tradingday[50] ;
	double begintime1;
	double begintime2;
	double begintime3;
	double begintime4;
	double endtime1;
	double endtime2;
	double endtime3;
	double endtime4;
	BOOL mInitOK;
	HANDLE mhSyncObj;
	CThostFtdcMdApi *mdapi;
	int RequestID;
	void WirteTick_Detail(const char * InstrumentID, const CThostFtdcDepthMarketDataField *pDepthMarketData);
	void WirteTick_Simple(const char * InstrumentID, const char * ticktime, double price);
	CMdSpi();
	~CMdSpi();

	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected()  ;

	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason)  ;

	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse) ;

	///登录请求响应
	virtual void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///登出请求响应
	virtual void OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)  ;

	///请求查询组播合约响应
	virtual void OnRspQryMulticastInstrument(CThostFtdcMulticastInstrumentField *pMulticastInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {};

	///错误应答
	virtual void OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)  ;

	///订阅行情应答
	virtual void OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)  ;

	///取消订阅行情应答
	virtual void OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)  ;

	///订阅询价应答
	virtual void OnRspSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {};

	///取消订阅询价应答
	virtual void OnRspUnSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {};

	///深度行情通知
	virtual void OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData) ;

	///询价通知
	virtual void OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp) ;

public:
 	BOOL InitMD();
	BOOL IsInitOK(){return mInitOK;}
	void SubscribeForQuoteRsp(char * InstrumentID);
	void SubscribeMarketData();
	void UnSubscribeMarketData();
	int ReqUserLogin();
	int ReqUserLogout();
	char * GetApiVersion();
	char *  GetTradingDay();
	void   RegisterFront(char *pszFrontAddress);
	void   RegisterNameServer(char *pszNsAddress);
private:
	bool IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo);

};
#endif
