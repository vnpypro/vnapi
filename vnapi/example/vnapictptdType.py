﻿# （1）我是quant林，早年供职于上海的量化私募，后于2017年推出的免费Python期货CTP接口开源框架，采用GPLV3开源协议，简单易学，
# 适合学习入门和拓展开发，就在1.0版发布后不久，我的vnapi 1.0（当时不叫vnapi）就遭受了非法金融经营者用1.2万字连载诽谤文的诋毁攻击，
# 他们甚至在诽谤文下方还放上他们自己的涉及非法金融开源软件的广告。
# 从那次诋毁到现在2.10版的重新推出已过了快7年，人生又有几个7年？
# 当初我作为技术创业者从未遇到类似的情况，不知如何应对，2017年7月我离开上海去了重庆，在重庆
# 写出了virtualapi仿真回测（盛立版、CTP版、通联沪深A股Level2版）、vntrader6框架雏形、kucps专业投资软件的代码，
# 每天开发工作完毕，就去万州的小巷点一份小火锅和啤酒，这是我最难忘的回忆，2019年我又回到了上海，
# 在最近刀郎事件发生后，我开始怀疑我当初离开上海的想法和刀郎的想法是一样的。
# 由于非法金融具有伪装性和渗透性，前几年利用开源开源旗号不断侵蚀我国合规金融市场，某币圈网红公开宣传非法金融，
# 其带有非法金融交易功能的软件甚至短暂的上了某期货公司官网，后他们又于2021年11月被上海证监局调查。
# 在过去7年中，我在打击非法金融的战线上贡献了自己的力量，付出了大量时间和精力的代价，说不清楚这样做是为了自救，
# 还是别的原因，但我忠心的希望金融从业者们牢记合规底线，勿给非法金融可乘之机。
# 为记住那段历史，本次发布的vnapi2.10 期货CTP接口底层代码，正是源于2017年4月的1.0开源版本的稍作整理，已升级到最新的CTP版本，
# 1.0版就是被诋毁造谣的那个版本，vnapi改名前的1.0 版本曾被北京大学出版社《python3.x全栈开发》收录并重点介绍，
# 现在vnapi2.0作为一款Python CTP接口的框架，和当初1.0一样，是基于Ctypes技术，简单易学，主要目的是为了为开发者
# 提供一个通俗易懂的python后端开源框架，并非为了提供一款面面俱到的商业产品，适合入门学习和在此基础上继续拓展开发。
# 我还采用PyQt技术开发为前端开发的 vntrader6客户端（https://www.vnpy.cn）， 基于我开发的vnapi的另一个回调版本，
# 面向的是专业机构客户，VNTrader系列可以说在vnapi 1.0后端框架技术上迭代产生的，
# 当初的1.0，和今天发布的2.0，以及VNTrader、Virtualapi产品均属于我和上海量贝软件的原创产品，
# 我于2014年还获得基于AR增强现实技术的国家发明专利一项。
# 我另有正在申请3项量化交易发明专利，这些技术已授予用于上海量贝作为技术积累可用于商业技术开发。
# 也希望谣言发布者们知晓，靠谣言诋毁一个执迷于技术的程序员和技术创业者是不可能成功的。
# 承载着7年的维权经历与梦想，引来了vnapi2.10版本的发布，这个产品定位是作抛砖引玉之用，适合用户学习Python量化和扩展功能，
# 而非堆砌功能，请各位以宽容的眼光去对待他。
# （2）vnapi 遵循 开源协议GPL v3
# 简单得说：对基于GPL v3协议的源代码，若个人或机构仅仅是自己使用，则可以闭源。
# 若基于该开源代码，开发出程序或衍生产品用于商业行为则也需开源。
# 官方网站：http://www.vnpy.cn

from ctypes import *

class VNInstrument(Structure):
    _fields_ = [('InstrumentID', c_char * 81)]
    pass

# 自定义字段
class VNInsertOrder(Structure):
    _fields_ = [('InstrumentID', c_char * 81),
                ('ExchangeID', c_char * 9),
                ('Direction', c_char),
                ('OffsetFlag', c_char),
                ('PriceType', c_char),
                ('Price', c_double),
                ('Num', c_int32),
                ('AlgorithmicTradingType', c_int32)
                ]
    pass

# 自定义字段，相对CTP精简部分字段
class VNDEFTradingAccountField(Structure):
    _fields_ = [('BrokerID', c_char * 11),  # 经纪公司代码
                ('InvestorID', c_char * 13),  # 投资者代码
                ('TradingDay', c_char * 9),
                ('Prebalance', c_double),  # 静态权益
                ('Current', c_double),  # 动态权益
                ('Available', c_double),  # 可用权益
                ('Rate', c_double),  # 今日盈亏
                ('Positionrate', c_double),  # 仓位，风险度
                ('WithdrawQuota', c_double),  # 可取资金
                ('Commission', c_double)  # 手续费
                ]
    pass

# 原生CTP结构体字段
class VNDEFInvestorPosition(Structure):
    _fields_ = [
        ('reserve1', c_char * 31),  # 保留字段
        ('BrokerID', c_char * 11),  # 经纪公司代码
        ('InvestorID', c_char * 13),  # 投资者代码
        ('PosiDirection', c_char),  # 持仓多空方向
        ('HedgeFlag', c_char),  # 投机套保标志
        ('PositionDate', c_char),  # 持仓日期
        ('YdPosition', c_int32),  # 上日持仓
        ('Position', c_int32) ,   # 今日持仓
        ('LongFrozen', c_int32),   # 多头冻结
        ('ShortFrozen', c_int32),  # 空头冻结
        ('LongFrozenAmount', c_double),  # 开仓冻结金额
        ('ShortFrozenAmount', c_double),  # 开仓冻结金额
        ('OpenVolume', c_int32),   # 开仓量
        ('CloseVolume', c_int32),  # 平仓量
        ('OpenAmount', c_double),  # 开仓金额
        ('CloseAmount', c_double),  # 平仓金额
        ('PositionCost', c_double),  # 持仓成本
        ('PreMargin', c_double),  # 上次占用的保证金
        ('UseMargin', c_double),  # 占用的保证金
        ('FrozenMargin', c_double),  # 冻结的保证金
        ('FrozenCash', c_double),  # 冻结的资金
        ('FrozenCommission', c_double),  # 冻结的手续费
        ('CashIn', c_double),  # 资金差额
        ('Commission', c_double),  # 手续费
        ('CloseProfit', c_double),  # 平仓盈亏
        ('PositionProfit', c_double),  # 持仓盈亏
        ('PreSettlementPrice', c_double),  # 上次结算价
        ('SettlementPrice', c_double),  # 本次结算价
        ('TradingDay', c_char * 9),  # 本次结算价
        ('SettlementID',c_int32),  # 结算编号
        ('OpenCost', c_double),  # 开仓成本
        ('ExchangeMargin', c_double),  # 交易所保证金
        ('CombPosition', c_int32),  # 组合成交形成的持仓
        ('CombLongFrozen', c_int32),  # 组合多头冻结
        ('CombShortFrozen', c_int32),  # 组合空头冻结
        ('CloseProfitByDate', c_double),  # 逐日盯市平仓盈亏
        ('CloseProfitByTrade', c_double),  # 逐笔对冲平仓盈亏
        ('TodayPosition', c_int32),  # 今日持仓
        ('MarginRateByMoney', c_double),  # 保证金率
        ('MarginRateByVolume', c_double),  # 保证金率(按手数)
        ('StrikeFrozen', c_int32),  # 执行冻结
        ('StrikeFrozenAmount', c_double),  # 执行冻结金额
        ('AbandonFrozen', c_int32),  # 放弃执行冻结
        ('ExchangeID',  c_char * 9),  # 交易所代码
        ('YdStrikeFrozen', c_int32),  # 执行冻结的昨仓
        ('InvestUnitID', c_char * 17),  # 投资单元代码
        ('PositionCostOffset', c_double),  # 大商所持仓成本差值，只有大商所使用
        ('TasPosition', c_int32),  # tas持仓手数
        ('TasPositionCost', c_double) , # tas持仓成本
        ('InstrumentID', c_char * 81),  # 合约代码

    ]
    pass

# 原生CTP结构体字段
class VNCThostFtdcOrderField(Structure):
    _fields_ = [('BrokerID', c_char * 11),  # 经纪公司代码
                ('InvestorID', c_char * 13),  # 投资者代码
                ('reserve1', c_char * 31),  # 合约代码
                ('OrderRef', c_char * 13),  # 报单引用
                ('UserID', c_char * 16),  # 用户代码
                ('OrderPriceType', c_char * 1),  # 报单价格条件
                ('Direction', c_char * 1),  # 买卖方向
                ('CombOffsetFlag', c_char * 5),  # 组合开平标志
                ('CombHedgeFlag', c_char * 5),  # 组合投机套保标志
                ('LimitPrice', c_double),  # 价格
                ('VolumeTotalOriginal', c_int),  # 数量
                ('TimeCondition', c_char * 1),  # 有效期类型
                ('GTDDate', c_char * 9),  # GTD日期
                ('VolumeCondition', c_char * 1),  # 成交量类型
                ('MinVolume', c_int),  # 最小成交量
                ('ContingentCondition', c_char * 1),  # 触发条件
                ('StopPrice', c_double),  # 止损价
                ('ForceCloseReason', c_char * 1),  # 强平原因
                ('IsAutoSuspend', c_int),  # 自动挂起标志
                ('BusinessUnit', c_char * 21),  # 业务单元
                ('RequestID', c_int),  # 请求编号
                ('OrderLocalID', c_char * 13),  # 本地报单编号
                ('ExchangeID', c_char * 9),  # 交易所代码
                ('ParticipantID', c_char * 11),  # 会员代码
                ('ClientID', c_char * 11),  # 客户代码
                ('reserve2', c_char * 31),  # 合约在交易所的代码
                ('TraderID', c_char * 21),  # 交易所交易员代码
                ('InstallID', c_int32),  # 安装编号
                ('OrderSubmitStatus', c_char * 1),  # 报单提交状态
                ('NotifySequence', c_int32),  # 报单提示序号
                ('TradingDay', c_char * 9),  # 交易日
                ('SettlementID', c_int32),  # 结算编号
                ('OrderSysID', c_char * 21),  # 报单编号
                ('OrderSource', c_char * 1),  # 报单来源
                ('OrderStatus', c_char * 1),  # 报单状态
                ('OrderType', c_char * 1),  # 报单类型
                ('VolumeTraded', c_int32),  # 今成交数量
                ('VolumeTotal', c_int32),  # 剩余数量
                ('InsertDate', c_char * 9),  # 报单日期
                ('InsertTime', c_char * 9),  # 委托时间
                ('ActiveTime', c_char * 9),  # 激活时间
                ('SuspendTime', c_char * 9),  # 挂起时间
                ('UpdateTime', c_char * 9),  # 最后修改时间
                ('CancelTime', c_char * 9),  # 撤销时间
                ('ActiveTraderID', c_char * 21),  # 最后修改交易所交易员代码
                ('ClearingPartID', c_char * 11),  # 结算会员编号
                ('SequenceNo', c_int32),  # 序号
                ('FrontID', c_int32),  # 前置编号
                ('SessionID', c_int32),  # 会话编号
                ('UserProductInfo', c_char * 11),  # 用户端产品信息
                ('StatusMsg', c_char * 81),  # 状态信息
                ('UserForceClose', c_int32),  # 用户强评标志
                ('ActiveUserID', c_char * 16),  # 操作用户代码
                ('BrokerOrderSeq', c_int32),  # 经纪公司报单编号
                ('RelativeOrderSysID', c_char * 21),  # 相关报单
                ('ZCETotalTradedVolume', c_int32),  # 郑商所成交数量
                ('IsSwapOrder', c_int32),  # 互换单标志
                ('BranchID', c_char * 9),  # 营业部编号
                ('InvestUnitID', c_char * 17),  # 投资单元代码
                ('AccountID', c_char * 13),  # 资金账号
                ('CurrencyID', c_char * 4),  # 币种代码
                ('reserve3', c_char * 16),  # IP地址
                ('MacAddress', c_char * 21),  # Mac地址
                ('InstrumentID', c_char * 81),  # 合约代码
                ('ExchangeInstID', c_char * 81),  # 合约在交易所的代码
                ('IPAddress', c_char * 33),  # IP地址
                ]
    pass

# 原生CTP结构体字段
class VNCThostFtdcTraderField(Structure):
    _fields_ = [('BrokerID', c_char * 11),  # 经纪公司代码
                ('InvestorID', c_char * 13),  # 投资者代码
                ('reserve1', c_char * 31),  # 合约代码
                ('OrderRef', c_char * 13),  # 报单引用
                ('UserID', c_char * 16),  # 用户代码
                ('ExchangeID', c_char * 9),  # 交易所代码
                ('TradeID', c_char * 21),  # 成交编号
                ('Direction', c_char),  # 买卖方向
                ('OrderSysID', c_char * 21),  # 报单编号
                ('ParticipantID', c_char * 11),  # 会员代码
                ('ClientID', c_char * 11),  # 客户代码
                ('TradingRole', c_char),  # 交易角色
                ('reserve2', c_char * 31),  # 合约在交易所的代码
                ('OffsetFlag', c_char),  # 开平标志
                ('HedgeFlag', c_char),  # 投机套保标志
                ('Price', c_double),  # 价格
                ('Volume', c_int),  # 数量
                ('TradeDate', c_char * 9),  # 成交时期
                ('TradeTime', c_char * 9),  # 成交时间
                ('TradeType', c_char),  # 成交类型
                ('PriceSource', c_char),  # 成交价来源
                ('TraderID', c_char * 21),  # 交易所交易员代码
                ('OrderLocalID', c_char * 13),  # 本地报单编号
                ('ClearingPartID', c_char * 11),  # 结算会员编号
                ('BusinessUnit', c_char * 21),  # 业务单元
                ('SequenceNo', c_int),  # 序号
                ('TradingDay', c_char * 9),  # 交易日
                ('SettlementID', c_int),  # 结算编号
                ('BrokerOrderSeq', c_int),  # 经纪公司报单编号
                ('TradeSource', c_char),  # 成交来源
                ('InvestUnitID', c_char*17),  # 投资单元代码
                ('InstrumentID', c_char * 81),  # 合约代码
                ('ExchangeInstID', c_char * 81)  # 合约在交易所的代码
                ]

    pass

# 原生CTP结构体字段
class VNThostFtdcInstrumentField(Structure):
    _fields_ = [('reserve1', c_char * 31),  # 保留的无效字段
                ('ExchangeID', c_char * 9),  # 交易所代码
                ('InstrumentName', c_char * 21),  # 合约名称
                ('reserve2', c_char * 31),  # 保留的无效字段
                ('reserve3', c_char * 31),  # 保留的无效字段
                ('ProductClass',  c_char * 1),  # 产品类型
                ('DeliveryYear',  c_int32),  # 交割年份
                ('DeliveryMonth', c_int32),  # 交割月
                ('MaxMarketOrderVolume', c_int32),  # 市价单最大下单量
                ('MinMarketOrderVolume', c_int32),  # 市价单最小下单量
                ('MaxLimitOrderVolume',  c_int32),  # 限价单最大下单量
                ('MinLimitOrderVolume',  c_int32),  # 限价单最小下单量
                ('VolumeMultiple', c_int32),  # 合约数量乘数
                ('PriceTick',  c_double),  # 最小变动价位
                ('CreateDate', c_char*9),  # 创建日
                ('OpenDate',  c_char*9),  # 上市日
                ('ExpireDate',  c_char*9),  # 到期日
                ('StartDelivDate', c_char * 9),  # 开始交割日
                ('EndDelivDate', c_char * 9),  # 结束交割日
                ('InstLifePhase', c_char),  # 合约生命周期状态
                ('IsTrading', c_int32),  # 当前是否交易
                ('PositionType', c_char),  # 持仓类型
                ('PositionDateType', c_char),  # 持仓日期类型
                ('LongMarginRatio', c_double  ),  # 多头保证金率
                ('ShortMarginRatio', c_double ),  # 空头保证金率
                ('MaxMarginSideAlgorithm', c_char),  # 是否使用大额单边保证金算法
                ('reserve4', c_char * 31),  # 保留的无效字段
                ('StrikePrice', c_double),  # 执行价
                ('OptionsType', c_char),  # 期权类型
                ('UnderlyingMultiple', c_double),  # 合约基础商品乘数
                ('CombinationType', c_char),  # 组合类型
                ('InstrumentID', c_char * 81),  # 合约代码
                ('ExchangeInstID', c_char * 81),  # 合约在交易所的代码
                ('ProductID', c_char * 81) , # 产品代码
                ('UnderlyingInstrID', c_char * 81)  # 基础商品代码
                ]
    pass

# 原生CTP结构体字段，暂时未用到
class VNCThostFtdcSettlementInfoConfirmField222(Structure):
    _fields_ = [('BrokerID', c_char * 11),  # 经纪公司代码
                ('InvestorID', c_char * 13),  # 投资者代码
                ('ConfirmDate', c_char * 9),  # 确认日期
                ('ConfirmTime', c_char * 9),  # 确认时间
                ('SettlementID', c_int), # 确认时间
                ('AccountID', c_char * 13),  # 确认时间
                ('CurrencyID', c_char * 4)  # 确认时间
                ]
    pass


class VNCThostFtdcQueryMaxOrderVolumeField(Structure):
    _fields_ = [('BrokerID', c_char * 11),  # 经纪公司代码
                ('InvestorID', c_char * 13),  # 投资者代码
                #('InstrumentID', c_char * 81),  # 合约代码
                ('reserve1', c_char * 31),  # 合约代码
                ('Direction', c_char * 1),  # 买卖方向
                ('OffsetFlag', c_char * 1),  # 开平标志
                ('HedgeFlag', c_char * 1),  # 投机套保标志
                ('MaxVolume', c_int32),  # 最大允许报单数量
                ('ExchangeID', c_char * 9),  # 交易所代码
                ('InvestUnitID', c_char * 17),  # 投资单元代码
                ('InstrumentID', c_char * 81) # 合约代码
                ]
    pass


class VNCThostFtdcRspInfoField(Structure):
    _fields_ = [('ErrorID', c_int32),  # 错误代码
                ('ErrorMsg', c_char * 81)  # 错误信息
                ]
    pass


class VNCThostFtdcRspUserLoginField(Structure):
    _fields_ = [('TradingDay', c_char * 9),  # 交易日
                ('LoginTime', c_char * 9),  # 登录成功时间
                ('BrokerID', c_char * 11),  # 经纪公司代码
                ('UserID', c_char * 16),  # 用户代码
                ('SystemName', c_char * 41),  # 交易系统名称
                ('FrontID', c_int32),  # 前置编号
                ('SessionID', c_int32),  # 会话编号
                ('MaxOrderRef', c_char * 13),  # 最大报单引用
                ('SHFETime', c_char * 9),  # 上期所时间
                ('DCETime', c_char * 9),  # 大商所时间
                ('CZCETime', c_char * 9),  # 郑商所时间
                ('FFEXTime', c_char * 9),  # 中金所时间
                ('INETime', c_char * 9)  # 能源中心时间
                ]
    pass

class VNCThostFtdcUserLogoutField(Structure):
    _fields_ = [
                ('BrokerID', c_char * 11),  # 经纪公司代码
                ('UserID', c_char * 16)  # 用户代码
                ]
    pass


VN_OPT_TWAP = 1  # 时间加权平均价格（TWAP）算法交易
VN_OPT_VWAP = 2  # 成交量加权平均价格（VWAP）算法交易

VN_Long = 0  # 买
VN_Short = 1  # 卖

# Direction or bsflag
VN_D_Buy = '0'  # 买
VN_D_Sell ='1'  # 卖

# offsetFlag
VN_OF_Open = '0'  # 开仓
VN_OF_Close = '1'  # 平仓
VN_OF_ForceClose = '2'  # 强平
VN_OF_CloseToday = '3'  # 平今
VN_OF_CloseYesterday = '4'  # 平昨
VN_OF_ForceOff = '5'  # 强减
VN_OF_LocalForceClose = '6'  # 本地强平

# price type
VN_OPT_AnyPrice = '1'  # 任意价
VN_OPT_LimitPrice = '2'  # 限价
VN_OPT_BestPrice = '3'  # 最优价
VN_OPT_LastPrice = '4'  # 最新价
VN_OPT_LastPricePlusOneTicks = '5'  # 最新价浮动上浮1个ticks
VN_OPT_LastPricePlusTwoTicks = '6'  # 最新价浮动上浮2个ticks
VN_OPT_LastPricePlusThreeTicks = '7'  # 最新价浮动上浮3个ticks
VN_OPT_AskPrice1 = '8'  # 卖一价
VN_OPT_AskPrice1PlusOneTicks = '9'  # 卖一价浮动上浮1个ticks
VN_OPT_AskPrice1PlusTwoTicks = 'A'  # 卖一价浮动上浮2个ticks
VN_OPT_AskPrice1PlusThreeTicks = 'B'  # 卖一价浮动上浮3个ticks
VN_OPT_BidPrice1 = 'C'  # 买一价
VN_OPT_BidPrice1PlusOneTicks = 'D'  # 买一价浮动上浮1个ticks
VN_OPT_BidPrice1PlusTwoTicks = 'E'  # 买一价浮动上浮2个ticks
VN_OPT_BidPrice1PlusThreeTicks = 'F'  # 买一价浮动上浮3个ticks

VN_Dynamic = 1  # 动态止损
VN_Static = 2  # 静态止损

VN_Dynamic_Capital = 0  # 动态止损
VN_Static_Capital = 1  # 静态止损

VN_POSITION_Sell_Today = 9001  # 今日空单
VN_POSITION_Buy_Today = 9002  # 今日多单
VN_POSITION_Sell_History = 9003  # 非今日空单
VN_POSITION_Buy_History = 9004  # 非今日多单
VN_POSITION_Sell_All = 9005  # 所有空单
VN_POSITION_Buy_All = 9006  # 所有多单





